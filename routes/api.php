<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categories/{category}', function (App\Category $category) {
    return $category;
});

Route::get('teams/{team}', function (App\Team $team) {
    return $team;
});

Route::put('update-status/{id}/{model}/{field}', function($id, $modelName, $field) {

	$savedStatus = DB::table($modelName)->where('id', $id)->pluck($field)->first(); //getting saved is_active field value
	$statusToChange = ($savedStatus) ? 0 : 1; //changing is_active field value to save

	//updating table is_active field value
	DB::table($modelName)
            ->where('id', $id)
            ->update([$field => $statusToChange]);
	
	return null;
});