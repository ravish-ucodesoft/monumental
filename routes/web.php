<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::namespace('Admin')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
// });
Route::prefix('admin')->group(function () {
    Route::match(['get', 'post'], '/', 'Admin\UserController@login');
	Route::get('dashboard', 'Admin\UserController@dashboard')->middleware('auth');
	Route::get('logout', 'Admin\UserController@logout')->middleware('auth');
	Route::post('change-password', 'Admin\UserController@changePassword')->middleware('auth');
	Route::post('update-profile', 'Admin\UserController@updateProfile')->middleware('auth');
	Route::get('categories', 'Admin\CategoryController@index')->middleware('auth');
	Route::post('add-category', 'Admin\CategoryController@store')->middleware('auth');
	Route::post('edit-category', 'Admin\CategoryController@update')->middleware('auth');
	Route::get('teams', 'Admin\TeamController@index')->middleware('auth');
	Route::post('add-team', 'Admin\TeamController@store')->middleware('auth');
	Route::post('add-new-team', 'Admin\TeamController@storeNew')->middleware('auth');
	Route::post('edit-team', 'Admin\TeamController@update')->middleware('auth');
	Route::get('cms-pages', 'Admin\CmsPageController@index')->middleware('auth');
	Route::get('cms-page-edit/{id}', 'Admin\CmsPageController@edit')->middleware('auth');
	Route::post('cms-page-update', 'Admin\CmsPageController@update')->middleware('auth');
	Route::get('players', 'Admin\UserController@index')->middleware('auth');
	Route::get('teams/applied', 'Admin\TeamController@applied')->middleware('auth');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about-us', function (App\CmsPage $cmspage) {
	$pageData = $cmspage::findOrFail(1);
    return view('cms_page', ['pageData' => $pageData]);
});

Route::get('/terms', function (App\CmsPage $cmspage) {
	$pageData = $cmspage::findOrFail(2);
    return view('cms_page', ['pageData' => $pageData]);
});

Route::get('/privacy-policy', function (App\CmsPage $cmspage) {
	$pageData = $cmspage::findOrFail(3);
    return view('cms_page', ['pageData' => $pageData]);
});

Route::match(['get', 'post'], '/sign-in', 'HomeController@login');
Route::get('/sign-up', 'HomeController@signup');
Route::post('/sign-up-store', 'RegisterController@create');
Route::get('/logout', 'UserController@logout');
Route::get('/activate-account/{userId}/{token}', 'HomeController@activateAccount');

Route::get('/teams/list', 'TeamController@team');
Route::get('/teams/index', 'TeamController@index');
Route::post('/teams/apply', 'TeamController@apply');
Route::get('auth/logout', 'Auth\LoginController@logout');
Route::get('/profile', 'UserController@profile');
Route::get('/edit-profile/{id}', 'UserController@editProfile');
Route::match(['get', 'post'], '/user-update/{id}', 'UserController@editProfileSave');
Route::match(['get', 'post'], '/forgot-password', 'HomeController@forgotPassword');
Route::match(['get', 'post'], '/change-password', 'UserController@changePassword');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
