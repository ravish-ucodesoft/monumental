<?php

namespace App\Http\Middleware;

use Closure;

class IsUserAccessAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        session(['user_id' => $request->user()->id, 'user_first_name' => $request->user()->first_name]);
        //middle ware to handle authorization for front end users
        if ($request->user()->user_type == 'super_admin') {
            return redirect('/auth/logout');
        }

        return $next($request);
    }
}
