<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Factory as ValidationFactory;

class UpdateUserPassword extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'old_password_matcher',
            function ($attribute, $value, $parameters) {
                return Hash::check($this->input('old_password'), Auth::user()->password);
            },
            'Old password does not match.'
        );
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required_with_all:password,password_confirmation|old_password_matcher',
            'password' => 'required_with:old_password,password_confirmation|min:9|regex:/(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[a-zA-Z])[^\s]*$/|confirmed|different:old_password',
            'password_confirmation' => 'required_with:password|min:9',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'old_password.required' => 'Old password is required',
            'password.required' => 'New password is required',
            'password.min' => 'New password must be nine characters long atleast',
            'password.regex' => 'New password must contain at least one special character, one alphabet and one numeric value',
            'password.confirmed' => 'New password and confirmed password do not match',
            'password.different' => 'New password must be different than old password',
        ];
    }
}
