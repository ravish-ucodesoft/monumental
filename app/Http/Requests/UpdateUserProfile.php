<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentPath= Route::getFacadeRoot()->current()->uri();

        $emailRule = 'required|email|unique:users,email';
        if (Auth::check()) {
            $emailRule = 'required|email|unique:users,email,' . Auth::id();
        }

        return [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'phone' => 'nullable|numeric',
            'email' => $emailRule,
            'date_of_birth' => 'required',
            'zipcode' => 'nullable|numeric',
            'profile_pic' => 'mimes:png,jpg,jpeg'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'First name is required',
            'first_name.min' => 'First name must br three charactes long atleast',
            'last_name.required' => 'Last name is required',
            'last_name.min' => 'Last name must br three charactes long atleast',
            'phone.required' => 'Phone number is required',
            'phone.number' => 'Valid phone number must be provided',
            'email.required' => 'Email address is required',
            'email.email' => 'Valid email address must be provided',
            'date_of_birth' => 'Date of birth is required',
            'zipcode.numeric' => 'Valid zip code must be provided',
        ];
    }
}
