<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\User;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:player', 'isuseraccessallowed']);
    }

    public function index(Request $request) {

        $user = User::find(Auth::user()->id);

        $appliedTeams = [];
        if (!empty($user->teams->toArray())) {
            foreach ($user->teams->toArray() as $userAppliedTeams) {
                $appliedTeams[] = $userAppliedTeams['id'];
            }
        }

        $categories = false;
        if (!empty($request->category)) {
            $categories = $request->category;
        }

        $ppg = (!empty($request->ppg)) ? $request->ppg : false;
        $rpg = (!empty($request->rpg)) ? $request->rpg : false;
        $apg = (!empty($request->apg)) ? $request->apg : false;
        $spg = (!empty($request->spg)) ? $request->spg : false;
        $bpg = (!empty($request->bpg)) ? $request->bpg : false;
        $fg = (!empty($request->fg)) ? $request->fg : false;
        $ft = (!empty($request->ft)) ? $request->ft : false;
        $three_pt_fg = (!empty($request->three_pt_fg)) ? $request->three_pt_fg : false;
        
        //query to get the average of all the college points
        $teamPointsAverage = DB::table('teams')
                        ->select(
                            DB::raw('avg(ppg) as ppg_avg'),
                            DB::raw('avg(rpg) as rpg_avg'),
                            DB::raw('avg(apg) as apg_avg'),
                            DB::raw('avg(spg) as spg_avg'),
                            DB::raw('avg(bpg) as bpg_avg'),
                            DB::raw('avg(fg) as fg_avg'),
                            DB::raw('avg(ft) as ft_avg'),
                            DB::raw('avg(3_pt_fg) as three_ft_avg')
                        )
                        ->when($categories, function ($query) use ($categories) {
                            return $query->whereIn('category_name', $categories);
                        })
                        ->first();
        
        // dd($teamPointsAverage);

        $collection = collect([]);
        if ($ppg) {
            $ncaaDIPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'NCAA DI', 'ppg', 'PPG');
            $collection = $collection->merge($ncaaDIPPG->toArray());

            $ncaaDIIPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'NCAA DII', 'ppg', 'PPG');
            $collection = $collection->merge($ncaaDIIPPG->toArray());

            $ncaaDIIIPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'NCAA DIII', 'ppg', 'PPG');
            $collection = $collection->merge($ncaaDIIIPPG->toArray());

            $naiaPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'NAIA', 'ppg', 'PPG');
            $collection = $collection->merge($naiaPPG->toArray());

            $nccaaPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'NCCAA', 'ppg', 'PPG');
            $collection = $collection->merge($nccaaPPG->toArray());

            $uscaaPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'USCAA', 'ppg', 'PPG');
            $collection = $collection->merge($uscaaPPG->toArray());

            $accaPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'ACCA', 'ppg', 'PPG');
            $collection = $collection->merge($accaPPG->toArray());

            $njcaaPPG = $this->__getTeamsUnderCriteria($ppg, $teamPointsAverage->ppg_avg, 'NJCAA', 'ppg', 'PPG');
            $collection = $collection->merge($njcaaPPG->toArray());
        }

        if ($rpg) {
            $ncaaDIRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'NCAA DI', 'rpg', 'RPG');
            $collection = $collection->merge($ncaaDIRPG->toArray());

            $ncaaDIIRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'NCAA DII', 'rpg', 'RPG');
            $collection = $collection->merge($ncaaDIIRPG->toArray());

            $ncaaDIIIRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'NCAA DIII', 'rpg', 'RPG');
            $collection = $collection->merge($ncaaDIIIRPG->toArray());

            $naiaRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'NAIA', 'rpg', 'RPG');
            $collection = $collection->merge($naiaRPG->toArray());

            $nccaaRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'NCCAA', 'rpg', 'RPG');
            $collection = $collection->merge($nccaaRPG->toArray());

            $uscaaRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'USCAA', 'rpg', 'RPG');
            $collection = $collection->merge($uscaaRPG->toArray());

            $accaRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'ACCA', 'rpg', 'RPG');
            $collection = $collection->merge($accaRPG->toArray());

            $njcaaRPG = $this->__getTeamsUnderCriteria($rpg, $teamPointsAverage->rpg_avg, 'NJCAA', 'rpg', 'RPG');
            $collection = $collection->merge($njcaaRPG->toArray());
        }

        if ($apg) {
            $ncaaDIAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'NCAA DI', 'apg', 'APG');
            $collection = $collection->merge($ncaaDIAPG->toArray());

            $ncaaDIIAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'NCAA DII', 'apg', 'APG');
            $collection = $collection->merge($ncaaDIIAPG->toArray());

            $ncaaDIIIAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'NCAA DIII', 'apg', 'APG');
            $collection = $collection->merge($ncaaDIIIAPG->toArray());

            $naiaAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'NAIA', 'apg', 'APG');
            $collection = $collection->merge($naiaAPG->toArray());

            $nccaaAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'NCCAA', 'apg', 'APG');
            $collection = $collection->merge($nccaaAPG->toArray());

            $uscaaAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'USCAA', 'apg', 'APG');
            $collection = $collection->merge($uscaaAPG->toArray());

            $accaAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'ACCA', 'apg', 'APG');
            $collection = $collection->merge($accaAPG->toArray());

            $njcaaAPG = $this->__getTeamsUnderCriteria($apg, $teamPointsAverage->apg_avg, 'NJCAA', 'apg', 'APG');
            $collection = $collection->merge($njcaaAPG->toArray());
        }

        if ($spg) {
            $ncaaDISPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'NCAA DI', 'spg', 'SPG');
            $collection = $collection->merge($ncaaDISPG->toArray());

            $ncaaDIISPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'NCAA DII', 'spg', 'SPG');
            $collection = $collection->merge($ncaaDIISPG->toArray());

            $ncaaDIIISPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'NCAA DIII', 'spg', 'SPG');
            $collection = $collection->merge($ncaaDIIISPG->toArray());

            $naiaSPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'NAIA', 'spg', 'SPG');
            $collection = $collection->merge($naiaSPG->toArray());

            $nccaaSPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'NCCAA', 'spg', 'SPG');
            $collection = $collection->merge($nccaaSPG->toArray());

            $uscaaSPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'USCAA', 'spg', 'SPG');
            $collection = $collection->merge($uscaaSPG->toArray());

            $accaSPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'ACCA', 'spg', 'SPG');
            $collection = $collection->merge($accaSPG->toArray());

            $njcaaSPG = $this->__getTeamsUnderCriteria($spg, $teamPointsAverage->spg_avg, 'NJCAA', 'spg', 'SPG');
            $collection = $collection->merge($njcaaSPG->toArray());
        }

        if ($bpg) {
            $ncaaDIBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'NCAA DI', 'bpg', 'BPG');
            $collection = $collection->merge($ncaaDIBPG->toArray());

            $ncaaDIIBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'NCAA DII', 'bpg', 'BPG');
            $collection = $collection->merge($ncaaDIIBPG->toArray());

            $ncaaDIIIBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'NCAA DIII', 'bpg', 'BPG');
            $collection = $collection->merge($ncaaDIIIBPG->toArray());

            $naiaBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'NAIA', 'bpg', 'BPG');
            $collection = $collection->merge($naiaBPG->toArray());

            $nccaaBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'NCCAA', 'bpg', 'BPG');
            $collection = $collection->merge($nccaaBPG->toArray());

            $uscaaBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'USCAA', 'bpg', 'BPG');
            $collection = $collection->merge($uscaaBPG->toArray());

            $accaBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'ACCA', 'bpg', 'BPG');
            $collection = $collection->merge($accaBPG->toArray());

            $njcaaBPG = $this->__getTeamsUnderCriteria($bpg, $teamPointsAverage->bpg_avg, 'NJCAA', 'bpg', 'BPG');
            $collection = $collection->merge($njcaaBPG->toArray());
        }

        if ($fg) {
            $ncaaDIFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'NCAA DI', 'fg', 'FG');
            $collection = $collection->merge($ncaaDIFG->toArray());

            $ncaaDIIFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'NCAA DII', 'fg', 'FG');
            $collection = $collection->merge($ncaaDIIFG->toArray());

            $ncaaDIIIFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'NCAA DIII', 'fg', 'FG');
            $collection = $collection->merge($ncaaDIIIFG->toArray());

            $naiaFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'NAIA', 'fg', 'FG');
            $collection = $collection->merge($naiaFG->toArray());

            $nccaaFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'NCCAA', 'fg', 'FG');
            $collection = $collection->merge($nccaaFG->toArray());

            $uscaaFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'USCAA', 'fg', 'FG');
            $collection = $collection->merge($uscaaFG->toArray());

            $accaFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'ACCA', 'fg', 'FG');
            $collection = $collection->merge($accaFG->toArray());

            $njcaaFG = $this->__getTeamsUnderCriteria($fg, $teamPointsAverage->fg_avg, 'NJCAA', 'fg', 'FG');
            $collection = $collection->merge($njcaaFG->toArray());
        }

        if ($ft) {
            $ncaaDIFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'NCAA DI', 'ft', 'FT');
            $collection = $collection->merge($ncaaDIFT->toArray());

            $ncaaDIIFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'NCAA DII', 'ft', 'FT');
            $collection = $collection->merge($ncaaDIIFT->toArray());

            $ncaaDIIIFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'NCAA DIII', 'ft', 'FT');
            $collection = $collection->merge($ncaaDIIIFT->toArray());

            $naiaFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'NAIA', 'ft', 'FT');
            $collection = $collection->merge($naiaFT->toArray());

            $nccaaFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'NCCAA', 'ft', 'FT');
            $collection = $collection->merge($nccaaFT->toArray());
            
            $uscaaFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'USCAA', 'ft', 'FT');
            $collection = $collection->merge($uscaaFT->toArray());

            $accaFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'ACCA', 'ft', 'FT');
            $collection = $collection->merge($accaFT->toArray());

            $njcaaFT = $this->__getTeamsUnderCriteria($ft, $teamPointsAverage->ft_avg, 'NJCAA', 'ft', 'FT');
            $collection = $collection->merge($njcaaFT->toArray());
        }

        if ($three_pt_fg) {
            $ncaaDITFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'NCAA DI', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($ncaaDITFTFG->toArray());

            $ncaaDIITFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'NCAA DII', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($ncaaDIITFTFG->toArray());

            $ncaaDIIITFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'NCAA DIII', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($ncaaDIIITFTFG->toArray());

            $naiaTFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'NAIA', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($naiaTFTFG->toArray());

            $nccaaTFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'NCCAA', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($nccaaTFTFG->toArray());

            $uscaaTFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'USCAA', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($uscaaTFTFG->toArray());

            $accaTFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'ACCA', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($accaTFTFG->toArray());

            $njcaaTFTFG = $this->__getTeamsUnderCriteria($three_pt_fg, $teamPointsAverage->three_ft_avg, 'NJCAA', '3_pt_fg', '3-PTFG');
            $collection = $collection->merge($njcaaTFTFG->toArray());
        }


        $unique = $collection->unique();
        $teamIdsObj = $unique->values()->all();
        $teamIds = [];
        foreach ($teamIdsObj as $value) {
            $teamIds[] = $value->id;
        }

        if (($ppg || $rpg || $apg || $spg || $bpg || $fg || $ft || $three_pt_fg) && empty($teamIds)) {
            $teamIds[] = 0;
        }
        
        $teams = DB::table('teams')
                            ->select([
                                'id',
                                'team_name',
                                'conference',
                                'category_name',
                                'state',
                                'ppg',
                                'rpg',
                                'apg',
                                'spg',
                                'bpg',
                                'fg',
                                'ft',
                                "3_pt_fg as three_pt_fg"
                            ])
                            ->when($teamIds, function ($query) use ($teamIds) {
                                return $query->whereIn('id', $teamIds);
                            })
                            ->when($categories, function ($query) use ($categories) {
                                return $query->whereIn('category_name', $categories);
                            })
                            ->orderBy('ppg', 'desc')
                            ->get();

        $groupedTeamsWithCategory = [];
        if (!empty($teams->toArray())) {
            $collection = collect($teams->toArray());

            $groupedTeamsWithCategory = $collection->groupBy('category_name');
        }
        $categories = DB::table('teams')
                            ->pluck('category_name', 'category_name');
        return view('Team.index', ['teams' => $groupedTeamsWithCategory, 'categories' => $categories, 'appliedTeams' => $appliedTeams]);
    }

    public function team(Request $request) {
    	$sortBy = null;
        $sortDir = null;
        $search = null;
        // echo '<pre>';print_r($request->all());die;
        if (!empty($request->sort)) {
            $sortBy = $request->sort;
            $sortDir = $request->order;
        }

        $teams = DB::table('teams')
        					->select([
        						'team_name',
        						'category_name',
        						'ppg',
        						'rpg',
        						'apg',
        						'spg',
        						'bpg',
        						'fg',
        						'ft',
        						"3_pt_fg as three_pt_fg"
        					])
                           	->where('category_name', 'LIKE', '%' . $request->category_name)
                            ->when($sortBy, function ($query) use ($sortBy, $sortDir) {
                                return $query->orderBy($sortBy, $sortDir);
                            }, function ($query) {
                                return $query->orderBy('ppg', 'desc');
                            })
                            ->paginate(20);
        
        $categories = DB::table('teams')
                            ->pluck('category_name', 'category_name');
        return view('Team.team', ['teams' => $teams, 'categories' => $categories]);
    }

    public function apply(Request $request) {
        $userId = Auth::user()->id;
        $teamToApply = [];
        if (null !== $request->user_applied_teams) {
            foreach ($request->user_applied_teams as $key => $value) {
                $teamToApply[] = [
                    'team_id' => $value,
                    'user_id' => $userId,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            if (DB::table('user_applied_teams')->insert($teamToApply)) {
                flash('You have applied for the selected team(s) successfully!')->success();
                return back();
            }
            
            flash('Some errors occurred. Please try again!')->error();
            return back();
        }
        flash('No team selected!')->error();
        return back();
    }

    /**
     * Method __getTeamsUnderCriteria to get ncaa DI ppg filtered records
     *
     * @param $points number
     * @param $avgPoint number
     * @param $category string
     * @param $fieldName string
     * @param $criteriaName string
     *
     * @return query object
     */
    private function __getTeamsUnderCriteria($points, $avgPoint, $category, $fieldName, $criteriaName) {
        if ($points < config('app.DivisonMinCriteria.' . $category . '.' . $criteriaName)) {
            return DB::table('teams')->orWhere($fieldName, 0)->get();
        }

        $isPointsBelowAvg = ($points < $avgPoint) ? true : false;
        return DB::table('teams')
                        ->select([
                            'id'
                        ])
                        ->where('category_name', $category)
                        ->when($isPointsBelowAvg, function ($query) use ($avgPoint, $category, $fieldName, $criteriaName) {
                            return $query
                                        ->where($fieldName, '>=', config('app.DivisonMinCriteria.' . $category . '.' . $criteriaName))
                                        ->where($fieldName, '<=', $avgPoint)
                                        ->whereNotNull($fieldName);
                        }, function ($query) use ($avgPoint, $category, $fieldName, $criteriaName) {
                            return $query->where($fieldName , '>=', $avgPoint)
                                        ->where($fieldName, '>=', config('app.DivisonMinCriteria.' . $category . '.' . $criteriaName))
                                        ->whereNotNull($fieldName);
                        })
                        ->get();
    }
}
