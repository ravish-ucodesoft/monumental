<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortBy = null;
        $sortDir = null;
        $search = null;
        
        if (!empty($request->sort)) {
            $sortBy = $request->sort;
            $sortDir = $request->order;
        }

        if (!empty($request->search)) {
            $search = $request->search;
        }

        $categories = DB::table('categories')
                                            ->where('name', 'like', '%' . $search . '%')
                                            ->when($sortBy, function ($query) use ($sortBy, $sortDir) {
                                                return $query->orderBy($sortBy, $sortDir);
                                            }, function ($query) {
                                                return $query->orderBy('name', 'desc');
                                            })
                                            ->paginate(20);
        return view('admin.Category.index', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories'
        ]);

        //inserting new category record into the database
        $category = new Category;

        $category->name = $request->name;

        if ($category->save()) {
            flash('New category has been added successfully!')->success();
            return redirect('/admin/categories');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name,' . $request->id
        ]);

        //updating new category record into the database
        $category = Category::find($request->id);
        
        $category->name = $request->name;

        if ($category->save()) {
            flash('Category has been updated successfully!')->success();
            return redirect('/admin/categories');
        }
    }

}
