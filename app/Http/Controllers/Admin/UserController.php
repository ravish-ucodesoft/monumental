<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserPassword;
use App\Http\Requests\UpdateUserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use Matriphe\Imageupload\ImageuploadFacade as Imageupload;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
	/**
     * Login method to super admin user
     *
     * @param  Request  $request
     * @return void
     */
    public function login(Request $request) {
    	
    	if ($request->isMethod('post')) {
    		$email = $request->input('email');
    		$password = $request->input('password');
    		if (Auth::guard('super_admin')->attempt(['email' => $email, 'password' => $password, 'user_type' => 'super_admin', 'active' => 1])) {
    			return redirect()->intended('/admin/dashboard');
	        }
    		flash('Invalid username or passoword.')->error();
    	}
    	return view('admin.login');
    }

    /**
     * Method dashboard to show the super admin dashboard page
     *
     * @return void
     */
    public function dashboard() {
    	
    	$totalUsers = DB::table('users')
                        ->where('user_type', '<>', 'super_admin')
                        ->count();

        $totalTeams = DB::table('teams')->count();
		
    	return view('admin.dashboard', ['totalUsers' => $totalUsers, 'totalTeams' => $totalTeams]);
    }

    /**
     * Method logout to logout super admin panel
     *
     * @return void
     */
    public function logout() {
    	Auth::logout();
    	flash('You have been logged out successfully!')->success();
    	return redirect('/admin');
    }

    /**
     * Method change password to change super admin password
     *
     * @param  UpdateUserPassword  $request
     * @return void
     */
    public function changePassword(UpdateUserPassword $request) {
    	if ($request->isMethod('post')) {
    		$userId = Auth::id();
    		if (DB::table('users')
	            ->where('id', $userId)
	            ->update(['password' => bcrypt($request->input('password'))])) {
    			flash('Your passowrd has been changed successfully!')->success();
    			return back();
    		}
    		flash('Old password does not match. Try again!')->error();
	    	return back()->withInput();
    	}
    }

    /**
     * Method updateProfile to update super admin profile
     *
     * @param  UpdateUserPassword  $request
     * @return void
     */
    public function updateProfile(UpdateUserProfile $request) {
    	if ($request->isMethod('post')) {
    		$userId = Auth::id();
    		$user = User::find($userId);

			$user->first_name = $request->input('first_name');
			$user->last_name = $request->input('last_name');
			$user->phone = $request->input('phone');
			$user->address = $request->input('address');
			$user->email = $request->input('email');

			if ($request->hasFile('profile_pic')) { //uploading profile picture code
				$dirToUpload = $userId;
		        $data['result'] = Imageupload::upload($request->file('profile_pic'), '', $dirToUpload);
                if (!$data['result']) {
		        	flash('Some errors occurred while uploading profie picture. Please try again!')->error();
	    			return back();
		        }
                
		        $user->profile_pic =  $data['result']['dimensions']['square50']['filename'];
		    }

			if ($user->save()) {
				flash('Your profile has been updated successfully!')->success();
	    		return back();
			}

			flash('Some errors occurred while updating your profile. Try again!')->error();
	    	return back()->withInput();
    	}
    }

    /**
     * Display a listing of the user resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortBy = null;
        $sortDir = null;
        $search = null;
        
        if (!empty($request->sort)) {
            $sortBy = $request->sort;
            $sortDir = $request->order;
        }

        $client = new Client();
        $countries = Cache::remember('countries', 60, function () use($client) {
            $res = $client->get('http://country.io/names.json');
            $countriesArr = json_decode($res->getBody(), true);
            return $countriesArr;
        });
        
        $users = DB::table('users')
                            ->select(
                                'id',
                                'user_type',
                                'first_name',
                                'last_name',
                                'email',
                                'address',
                                'phone',
                                'date_of_birth',
                                'gender',
                                'zipcode',
                                'country',
                                'created_at',
                                'active'
                            )
                            ->where('user_type', '<>', 'super_admin')
                            ->where(function ($query) use($request) {
                                $query->where('first_name', 'LIKE', '%' . $request->search . '%')
                                    ->orWhere('last_name', 'LIKE', '%' . $request->search . '%')
                                    ->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'LIKE', '%' . $request->search . '%')
                                    ->orWhere('email', 'LIKE', '%' . $request->search . '%')
                                    ->orWhere('phone', 'LIKE', '%' . $request->search . '%');
                            })
                            ->when($sortBy, function ($query) use ($sortBy, $sortDir) {
                                return $query->orderBy($sortBy, $sortDir);
                            }, function ($query) {
                                return $query->orderBy('first_name', 'asc');
                            })
                            ->paginate(20);
        // echo '<pre>';print_r($users);die;
        return view('admin.User.index', ['users' => $users, 'countries' => $countries]);
    }
}
