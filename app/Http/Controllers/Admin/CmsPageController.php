<?php

namespace App\Http\Controllers\Admin;

use App\CmsPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CmsPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortBy = null;
        $sortDir = null;
        $search = null;
        
        if (!empty($request->sort)) {
            $sortBy = $request->sort;
            $sortDir = $request->order;
        }

        $cmsPages = DB::table('cms_pages')
                           	->where('page_name', 'LIKE', $request->search . '%')
                           	->orWhere('meta_keywords', 'LIKE', '%' . $request->search)
							->orWhere('meta_description', 'LIKE', '%' . $request->search)
                            ->when($sortBy, function ($query) use ($sortBy, $sortDir) {
                                return $query->orderBy($sortBy, $sortDir);
                            }, function ($query) {
                                return $query->orderBy('page_name', 'asc');
                            })
                            ->get();
        
        return view('admin.CmsPage.index', ['cmsPages' => $cmsPages]);
    }

    /**
     * open edit page of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    	$cmsPage = DB::table('cms_pages')
                           	->where('id', $id)
                            ->first();
// echo '<pre>';print_r($cmsPage);die;        
        return view('admin.CmsPage.edit', ['cmsPage' => $cmsPage]);
    }

    /**
     * open edit page of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'page_title' => 'required',
            'page_name' => 'required',
            'meta_description' => 'required',
        ]);

        //updating new category record into the database
        $cmsPage = CmsPage::find($request->id);
        
        $cmsPage->meta_title = $request->meta_title;
        $cmsPage->meta_keywords = $request->meta_keywords;
        $cmsPage->page_title = $request->page_title;
        $cmsPage->page_name = $request->page_name;
        $cmsPage->meta_description = $request->meta_description;

    	// echo '<pre>';print_r($cmsPage);die;
        if ($cmsPage->save()) {
            flash('Cms has been updated successfully!')->success();
            return redirect('/admin/cms-pages');
        }
    }
}
