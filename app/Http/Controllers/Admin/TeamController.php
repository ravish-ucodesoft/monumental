<?php

namespace App\Http\Controllers\Admin;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Excel;
use Illuminate\Support\Collection;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortBy = null;
        $sortDir = null;
        $search = null;
        
        if (!empty($request->sort)) {
            $sortBy = $request->sort;
            $sortDir = $request->order;
        }

        $teams = DB::table('teams')
                           	->where('team_name', 'LIKE', $request->search . '%')
							->where('category_name', 'LIKE', '%' . $request->category_name)
                            ->when($sortBy, function ($query) use ($sortBy, $sortDir) {
                                return $query->orderBy($sortBy, $sortDir);
                            }, function ($query) {
                                return $query->orderBy('team_name', 'asc');
                            })
                            ->paginate(20);
        $categories = DB::table('teams')
                            ->pluck('category_name', 'category_name');
        return view('admin.Team.index', ['teams' => $teams, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
            'teams_import' => 'required'
        ]);

    	$path = $request->file('teams_import')->getRealPath();
    	$excelData = Excel::load($path)->get();
    	$teamDataToSave = [];

        try {
        	if($excelData->count()){
                foreach ($excelData as $key => $value) {
                    foreach ($value->toArray() as $row) {
                    	// echo '<pre>';print_r($row);die;
                    	if (!empty($row['teams'])) {
    		         		$rowData['team_name'] = $row['teams'];
                    	}

    		         	$rowData['category_name'] = $value->getTitle();
    		         	$rowData['conference'] = $row['conference'];
    		         	$rowData['state'] = $row['state'];
    		         	if (array_has($row, 'region')) {
    		         		$rowData['region'] = $row['region'];
    		         	}

    		         	$rowData['fr'] = $row['fr'];
    		         	$rowData['so'] = $row['so'];
    		         	$rowData['ju'] = $row['ju'];
    		         	$rowData['se'] = $row['se'];
    		         	$rowData['ppg'] = $row['ppg'];
    		         	$rowData['rpg'] = $row['rpg'];
    		         	$rowData['apg'] = $row['apg'];
    		         	$rowData['spg'] = $row['spg'];
    		         	$rowData['bpg'] = $row['bpg'];
    		         	$rowData['fg'] = $row['fg'];
    		         	$rowData['ft'] = $row['ft'];
    		         	$rowData['3_pt_fg'] = $row['3_pt_fg'];
    		         	$rowData['created_at'] = date('Y-m-d H:i:s');
    		         	$rowData['updated_at'] = date('Y-m-d H:i:s');
    		         	$teamDataToSave[] = $rowData;
    		        }
                }

                if(!empty($teamDataToSave)){
                    DB::table('teams')->insert($teamDataToSave);
                    flash('Teams excel has been imported successfully!')->success();
        			return redirect('/admin/teams');
                }
            }
        } catch (\ErrorException $e) {
            flash('File did not upload. Kindly upload the correct file format to make it uploaded properly.')->error();
            return redirect('/admin/teams');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNew(Request $request)
    {
        // echo '<pre>';print_r($request->all());die;
        $this->validate($request, [
            'team_name' => 'required|unique:teams,team_name,' . $request->id,
            'category_name' => 'required|min:2',
            'state' => 'nullable|min:2',
            'ppg' => 'nullable|numeric',
            'rpg' => 'nullable|numeric',
            'spg' => 'nullable|numeric',
            'bpg' => 'nullable|numeric',
            'fg' => 'nullable|numeric',
            'ft' => 'nullable|numeric',
            '3_pt_fg' => 'nullable|numeric'
        ]);

        try {
            $team = new Team;
            
            $team->team_name = $request->team_name;
            $team->category_name = $request->category_name;
            $team->state = $request->state;
            $team->region = $request->region;
            $team->ppg = $request->ppg;
            $team->rpg = $request->rpg;
            $team->spg = $request->spg;
            $team->bpg = $request->bpg;
            $team->fg = $request->fg;
            $team->ft = $request->ft;
            $team['3_pt_fg'] = $request['3_pt_fg'];

            if ($team->save()) {
                flash('New team has added updated successfully!')->success();
                return back();
            }
        } catch (\QueryException $e) {
            flash('Some errors occurred, please try again.')->error();
        } catch (\PDOException $e) {
            flash('Some errors occurred, please try again.')->error();
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $this->validate($request, [
            'team_name' => 'required|unique:teams,team_name,' . $request->id,
            'category_name' => 'required|min:2',
            'state' => 'nullable|min:2',
            'ppg' => 'nullable|numeric',
            'rpg' => 'nullable|numeric',
            'apg' => 'nullable|numeric',
            'spg' => 'nullable|numeric',
            'bpg' => 'nullable|numeric',
            'fg' => 'nullable|numeric',
            'ft' => 'nullable|numeric',
            '3_pt_fg' => 'nullable|numeric'
        ]);
        
        //updating team record into the database
        $team = Team::find($request->id);
        
        $team->team_name = $request->team_name;
        $team->category_name = $request->category_name;
        $team->state = $request->state;
        $team->region = $request->region;
        $team->ppg = $request->ppg;
        $team->rpg = $request->rpg;
        $team->apg = $request->apg;
        $team->spg = $request->spg;
        $team->bpg = $request->bpg;
        $team->fg = $request->fg;
        $team->ft = $request->ft;
        $team->updated_at = date('Y-m-d H:i:s');
        $team['3_pt_fg'] = $request['3_pt_fg'];

        if ($team->save()) {
            flash('Team has been updated successfully!')->success();
            return back();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function applied(Request $request)
    {
        $sortBy = null;
        $sortDir = null;
        $search = null;
        
        if (!empty($request->sort)) {
            $sortBy = $request->sort;
            $sortDir = $request->order;
        }

        $teams = DB::table('user_applied_teams')
            ->join('users', 'users.id', '=', 'user_applied_teams.user_id')
            ->join('teams', 'teams.id', '=', 'user_applied_teams.team_id')
            ->select('user_applied_teams.*', DB::raw('CONCAT_WS(" ", users.first_name, users.last_name) AS full_name'), 'users.first_name', 'users.last_name', 'teams.team_name')
            ->where(function ($query) use($request) {
                $query->where('users.first_name', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('users.last_name', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('teams.team_name', 'LIKE', '%' . $request->search . '%')
                    ->orWhere(DB::raw('CONCAT_WS(" ", users.first_name, users.last_name)'), 'LIKE', '%' . $request->search . '%');
            })
            ->when($sortBy, function ($query) use ($sortBy, $sortDir) {
                return $query->orderBy($sortBy, $sortDir);
            }, function ($query) {
                return $query->orderBy('user_applied_teams.created_at', 'asc');
            })
            ->paginate(20);

        return view('admin.Team.applied', ['teams' => $teams]);
    }
}
