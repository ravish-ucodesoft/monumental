<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use \Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('web');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function login() {
        return view('Home.signin');
    }

    public function signup() {


        $client = new Client();
        Cache::remember('countries', 60, function () use($client) {
            $res = $client->get('http://country.io/names.json');
            $countriesArr = json_decode($res->getBody(), true);
            return $countriesArr;
        });

        return view('Home.signup');
    }

    /**
     * update active field in user resource.
     *
     * @param  string  $userId
     * @param string $token
     * @return null
     */
    public function activateAccount($userId, $token) {
        
        $userId = base64_decode($userId);

        $user = User::where('id', $userId)
                ->where('token', $token)
                ->first();
        
        if (empty($user)) {
            flash('This link has been expired.')->error();
            return redirect('/');
        }
        
        $user->active = true;
        $user->token = str_random(20);

        if ($user->save()) {
            flash('Your account has been activated successfully. Kindly proceed to login now.')->success();
            return redirect('/');
        }

        flash('Some error occurred while activating your account. Please try again!')->error();
        return redirect('/');
    }

    public function forgotPassword(Request $request) {
        // dd($request->all());die;
        return view('Home.forgot_password');
    }
}
