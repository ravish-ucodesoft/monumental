<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserProfile;
use App\Http\Requests\UpdateUserPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use \Cache;

class UserController extends Controller
{

	public function __construct()
    {
        $this->middleware(['auth:player', 'isuseraccessallowed']);
    	$client = new Client();
        Cache::remember('countries', 60, function () use($client) {
            $res = $client->get('http://country.io/names.json');
            $countriesArr = json_decode($res->getBody(), true);
            return $countriesArr;
        });
        
    }
    
    public function profile() {
    	return view('User.profile');
    }

    public function editProfile($id) {


    	$user = User::find($id);
    	return view('User.edit_profile', ['user' => $user]);
    }

    public function editProfileSave(UpdateUserProfile $request, $id) {
    	$user = User::find($id);

    	$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->phone = $request->input('phone');
		$user->email = $request->input('email');
		$user->gender = $request->input('gender');
		$user->date_of_birth = $request->input('date_of_birth');
		$user->zipcode = $request->input('zipcode');
		$user->country = $request->input('country');

		if ($user->save()) {
			flash('Your profile has been updated successfully!')->success();
    		return redirect('/profile');
		}

		flash('Some errors occurred while updating your profile. Try again!')->error();
    	return back();
    }

    public function changePassword(UpdateUserPassword $request) {
        if ($request->isMethod('post')) {
            $userId = Auth::id();
            if (DB::table('users')
                ->where('id', $userId)
                ->update(['password' => bcrypt($request->input('password'))])) {
                flash('Your passowrd has been changed successfully!')->success();
                return back();
            }
            return back()->withInput();
        }
        return view('User.change_password');
    }

    /** 
    * Method logout to logout user form the application
    *
    * @return void
    */
    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect('/');
    }
}
