<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Mail\UserRegisteration;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/sign-in';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function guard()
    {
        return Auth::guard('player');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'first_name.required' => 'First name is required',
            'first_name.min' => 'First name must br three charactes long atleast',
            'last_name.required' => 'Last name is required',
            'last_name.min' => 'Last name must br three charactes long atleast',
            'phone.numeric' => 'Valid phone number must be provided',
            'email.required' => 'Email address is required',
            'email.email' => 'Valid email address must be provided',
            'password.required' => 'New password is required',
            'password.min' => 'New password must be nine characters long atleast',
            'password.regex' => 'New password must contain at least one special character, one alphabet and one numeric value',
            'password.confirmed' => 'New password and confirmed password do not match',
            'date_of_birth' => 'Date of birth is required',
            'gender' => 'Gender is required',
            'zipcode.numeric' => 'Valid zip code must be provided',
            'user_type' => 'Role is required',
            'agree' => 'Terms and privacy policy must be agreed',
        ];

        return Validator::make($data, [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'phone' => 'nullable|numeric',
            'email' => 'required|email|unique:users,email',
            'password' => 'required_with:password_confirmation|min:9|regex:/(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[a-zA-Z]).*$/|confirmed',
            'password_confirmation' => 'required_with:password|min:9',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'zipcode' => 'nullable|numeric',
            'user_type' => 'required',
            'agree' => 'required',
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone' => $data['phone'],
            'date_of_birth' => $data['date_of_birth'],
            'gender' => $data['gender'],
            'zipcode' => $data['zipcode'],
            'country' => $data['country'],
            'user_type' => $data['user_type'],
            'token' => str_random(20),
            'active' => $data['active']
        ]);
        return $user;
    }

    public function register(Request $request)
    {
        $token = str_random(20);
        $request->request->add([
            'token' => $token,
            'active' => true,
            'activation_link' => asset('/activate-account/' . $token)
        ]);
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        
        $user->activation_link = asset('/activate-account/' . base64_encode($user->id) . '/' . $user->token);
        
        //sending email code
        // Mail::to($user->email)->send(new UserRegisteration($user));
        // flash('Your account has been created successfully at monumental.com. Kindly activate your account by clicking on the activate link sent to your email address.')->success();
        flash('Your account has been created successfully at monumental.com. Kindly proceed to login now.')->success();
        return redirect($this->redirectPath());
    }
}
