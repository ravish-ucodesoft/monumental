<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $sortable = ['id',
	                    'name',
	                    'created_at',
	                    'updated_at'];
}
