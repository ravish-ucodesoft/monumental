<?php

use Illuminate\Database\Seeder;

class CmsPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms_pages')->insert([
	            'meta_title' => 'about us',
	            'meta_keywords' => 'about us',
	            'page_title' => 'about_us',
	            'page_name' => 'About Us',
	            'meta_description' => 'The Walt Disney Company, Red Bull GmbH, and Nike, Inc. each share something in common: major investments in sports marketing and development.

	Monumental Athletes Management is a sports agency with a goal of transcending the sports industry in The Bahamas, the Caribbean community, and ultimately, the world.

	Monumental takes a professional approach at enhancing athletes, coaches, leagues, and businesses. We believe sports is more than just a game; it\'s educational & economic development with the opportunity to positively impact communities',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
        	]);
        DB::table('cms_pages')->insert([
	            'meta_title' => 'terms',
	            'meta_keywords' => 'terms',
	            'page_title' => 'terms',
	            'page_name' => 'Terms',
	            'meta_description' => 'The Walt Disney Company, Red Bull GmbH, and Nike, Inc. each share something in common: major investments in sports marketing and development.

		Monumental Athletes Management is a sports agency with a goal of transcending the sports industry in The Bahamas, the Caribbean community, and ultimately, the world.

		Monumental takes a professional approach at enhancing athletes, coaches, leagues, and businesses. We believe sports is more than just a game; it\'s educational & economic development with the opportunity to positively impact communities',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
        	]);
        DB::table('cms_pages')->insert([
	            'meta_title' => 'privacy policy',
	            'meta_keywords' => 'privacy policy',
	            'page_title' => 'privacy policy',
	            'page_name' => 'Privacy Policy',
	            'meta_description' => 'The Walt Disney Company, Red Bull GmbH, and Nike, Inc. each share something in common: major investments in sports marketing and development.

		Monumental Athletes Management is a sports agency with a goal of transcending the sports industry in The Bahamas, the Caribbean community, and ultimately, the world.

		Monumental takes a professional approach at enhancing athletes, coaches, leagues, and businesses. We believe sports is more than just a game; it\'s educational & economic development with the opportunity to positively impact communities',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
        	]);
    }
}
