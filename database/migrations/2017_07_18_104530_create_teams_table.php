<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name');
            $table->string('team_name');
            $table->string('conference')->nullable();
            $table->string('state')->nullable();
            $table->string('region')->nullable();
            $table->decimal('fr', 10,3)->nullable();
            $table->decimal('so', 10,3)->nullable();
            $table->decimal('ju', 10,3)->nullable();
            $table->decimal('se', 10,3)->nullable();
            $table->decimal('ppg', 10,3)->nullable();
            $table->decimal('rpg', 10,3)->nullable();
            $table->decimal('apg', 10,3)->nullable();
            $table->decimal('spg', 10,3)->nullable();
            $table->decimal('bpg', 10,3)->nullable();
            $table->decimal('fg', 10,3)->nullable();
            $table->decimal('ft', 10,3)->nullable();
            $table->decimal('3_pt_fg', 10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
