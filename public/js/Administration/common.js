(function () {

    PNotify.prototype.options.styling = "fontawesome";

    Vue.component('modal', {
      template: '#modal-template-to-edit'
    })
	
    new Vue({
        el: "#vue-container",
        data: {
            teamId: '',
            teamName: '',
            teamCategory: '',
            teamConference: '',
            teamState: '',
            teamRegion: '',
            teamPPG: '',
            teamRPG: '',
            teamAPG:'',
            teamSPG: '',
            teamBPG: '',
            teamFG: '',
            teamFT: '',
            team3PTFG: '',
            showModal: false
        },
        methods:
        {
            onUpdateStatus: function (url){
                var self = $(event.target);
                var self_status =
                this.$http.put(url).then(response => {
                    if (response.status) {
                        show_stack_bar_top('success', 'Status has been updated successfully.', 'Success');
                        return true;
                    }

                }, response => {
                    revertSwitchState(self);
                    show_stack_bar_top('error', 'Some errors occurred while updating status. Please try again', 'Error');
                });
            },
            ediTeam: function (url){
                let thisEl = this;
                this.$http.get(url).then(response => {
                    thisEl.teamId = response.body.id;
                    thisEl.teamName = response.body.team_name;
                    thisEl.teamCategory = response.body.category_name;
                    thisEl.teamConference = response.body.conference;
                    thisEl.teamState = response.body.state;
                    thisEl.teamRegion = response.body.region;
                    thisEl.teamPPG = response.body.ppg;
                    thisEl.teamRPG = response.body.rpg;
                    thisEl.teamAPG = response.body.apg;
                    thisEl.teamSPG = response.body.spg;
                    thisEl.teamBPG = response.body.bpg;
                    thisEl.teamFG = response.body.fg;
                    thisEl.teamFT = response.body.ft;
                    thisEl.team3PTFG = response.body['3_pt_fg'];
                    
                    thisEl.showModal = true;

                }, response => {
                    show_stack_bar_top('error', 'Some errors occurred while getting date for team update. Please try again', 'Error');
                });
            }
        }
    });

    var revertSwitchState = function(element) {
        let checked = element.prop('checked');
        element.prop('checked', !checked);
    }

})();

function calculateSideBarHeight() {
	if ($(window).width() < 1024) {
        $("body").addClass('sidebar-collapse');
    }
    var equalto = $("body").innerHeight();
    $(".main-sidebar").innerHeight(equalto);
}   
calculateSideBarHeight();

function show_stack_bar_top(type,msg,title) {
	new PNotify({
        title: title,
        text: msg,
        type: type
    });
}