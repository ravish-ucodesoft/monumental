(function () {
	$(document).on('click', '.edit-category', function(e) {
		e.preventDefault();
		var element = $(this);
		var url = element.attr('href');
		$.ajax({
            url: url,
            success: function(resp) {
            	console.log(resp);
            	$('.edit-category-value').val(resp.name);
            	$('input[type="hidden"][name="id"]').val(resp.id);
            	$('#edit-category').modal('show');
            }
        });
	});

})();