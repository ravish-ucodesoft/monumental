(function () {

    new Vue({
        el: "#vue-teams",
        methods:
        {
            markChecked: function (){
                let self = $(event.target);
                self.parents('table').children('tbody').find('input').prop('checked', false);
                if (self.is(':checked')) {
                    self.parents('table').children('tbody').find('input').prop('checked', true);
                }
            },
            markParentChecked: function() {
                let self = $(event.target);
                let checkboxLength = self.parents('tbody').find('input').length;
                let checkboxCheckedLength = self.parents('tbody').find('input:checked').length;
                console.log(checkboxLength,checkboxCheckedLength);
                if (checkboxCheckedLength == checkboxLength) {
                    self.parents('table').children('thead').find('input').prop('checked', true);
                    return true;
                }
                self.parents('table').children('thead').find('input').prop('checked', false);
            }
        }
    });
})();