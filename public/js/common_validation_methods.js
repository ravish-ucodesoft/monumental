(function () {
    $.validator.addMethod("pwcheckallowedchars", function (value) {
        return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)
    }, "The password must contain at least one special character");

    $.validator.addMethod("pwcheckalphabets", function (value) {
        return /[a-zA-Z]/.test(value) // has a alphabet
    }, "The password must contain at least one alphabet");

    $.validator.addMethod("pwchecknumerics", function (value) {
        return /[0-9]/.test(value) // has a lowercase letter
    }, "The password must contain at least numeric value.");

    $.validator.addMethod("validphoneno", function(value, element) {
        return this.optional(element) || /^((\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[0-9]((\s|\s?\-\s?)?[0-9]){9}$/.test(value);
    }, "Please specify a valid phone number.");
})();
function password_validate(element) {
    element.validate({
        rules: {
            'old_password' : {
                'required': true
            },
            'password': {
                'required': true,
                'minlength': 9,
                'pwcheckallowedchars': true,
                'pwcheckalphabets': true,
                'pwchecknumerics': true
            },
            'password_confirmation': {
                'required': true,
                'equalTo': '#new-admin-password'
            }
        },
        messages: {
            'old_password' : {
                'required': 'Old password must be entered.'
            },
            'password': {
                'required': 'New password must be entered.',
                'minlength': 'Password must be nine characters long.'
            },
            'password_confirmation': {
                'required': 'Confirm password must be entered.',
                'equalTo': 'Passowords do not match.'
            }
        }
    });
}