@extends('layouts.pages')

@section('title', 'Home')

@section('content')
    <!-- Begin page content -->
    <div class="container home">
        @include('flash::message')
    </div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div class="text-section">
            <h1>Smarter Sports</h1>
            <h3>Improving the Game</h3>
            <p>
            Disney, Red Bull, and Nike, each have major investments in sports marketing and development.  These companies have advanced the sports industry in their own way.  Monumental seeks to do the same.<p>

<p>Monumental Athletics Management is a sports marketing agency that will strive to transcend sports.</p>

<p>Monumental takes a professional approach at enhancing athletes, coaches, leagues, and businesses. We believe sports is more than just a game; it's educational and presents the opportunity for economic development which can positively impact communities.</p>
            </p>

        </div>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
    
            </div>
            <div class="item ">
    
            </div>
        </div>
    </div>
    <div class="our-motivation">
        <div class="container">
            <h1>Our Motivation </h1>
            <h3>Monumental Improvement</h3>
            <p>The Walt Disney Company, Red Bull GmbH, and Nike, Inc. each share something in common: major investments in sports marketing and development. </p>
            <p>Monumental Athletes Management is a sports agency with a goal of transcending the sports industry in The Bahamas, the Caribbean community, and ultimately, the world.</p>
            <p>Monumental takes a professional approach at enhancing athletes, coaches, leagues, and businesses. We believe sports is more than just a game; it's educational & economic development with the opportunity to positively impact communities</p>
        </div>
    </div>
    <div class="sports-marketing">
        <div class="container">
            <div class="col-sm-6">
                <h1>Sports Marketing</h1>
                <h4>Stand out with Sports</h4>
                <p>- Website & Content Management: Maximize your online presence with fresh and relevant content daily.</p>
                <p> Sponsorship Proposals: Whether you're an event organizer looking for corporate partnership or a business looking to utilize sports marketing to grow your company, Monumental will create win-win-win sponsorships for sports properties, businesses, and fan</p>
                <p>Content Creation: Each business and athlete has a story so allow Monumental to maximize yours.</p>
            </div>
            <div class="col-sm-6">
                {{ Html::image(asset('public/img/marketing.png'), "img-responsive") }}
            </div>
        </div>
    </div>
    <div class="sports-analytics">
        <div class="container">
            <div class="col-sm-6">{{ Html::image(asset('public/img/analytics.png'), "img-responsive") }}</div>
            <div class="col-sm-6">
                <h1>Sports Analytics</h1>
                <h4>Maximize Your Stats</h4>
                <p>- Stats Collection: Athletes, coaches, and leagues could have their stats properly collected and managed.</p>
                <p>- Statistical Analysis: "Men lie, women lie, numbers don't." How good of a coach or player are you? Let your numbers prove your ability</p>
                <p>Content Creation: Each business and athlete has a story so allow Monumental to maximize yours.</p>
            </div>
        </div>
    </div>
    <div class="get-recruited">
        <div class="container">
            <div class="col-sm-6 get-recruited-section">
                <h1>Get Recruited</h1>
                <h4>Future Collegiate Athletes</h4>
                <p>- Universities & colleges database: Target the right athletic programs and play in college.</p>
                <p>- Recruitment packages: Highlight your achievements and present them to college coaches & recruiters.</p>
                <p>- Tutoring: Increase your knowledge in your least favorite subject by learning through applied sports techniques examples.</p>
            </div>
            <div class="col-sm-6">
                {{ Html::image(asset('public/img/recruited.png'), "img-responsive") }}
            </div>
        </div>
    </div>
    <div class="opening-hours">
        <div class="container text-center">
            <div class="col-sm-12">
                <h1>Opening Hours</h1>
                <h4>At wherever you play!</h4>
                <p>"It ain't over 'til it's over." Yogi Berra</p>
            </div>
        </div>
    </div>
    <div class="contact-section">
        <div class="container">
            <div class="col-sm-6 contact-section">
                <h1>Contact</h1>
                <p>Connect x Communicate x Create</p>
                <p>Want to partner or volunteer with Monumental? Let me know!</p>
                <p>42-804-0621</p>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="email" class="form-control" >  
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" >           
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Subject</label>
                    <input type="email" class="form-control" >
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="email" class="form-control" >       
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input type="email" class="form-control" >
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-orange btn-block">Send</button>
                </div>
            </div>
        </div>
    </div>
@endsection