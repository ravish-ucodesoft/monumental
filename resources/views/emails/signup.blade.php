@component('mail::message')
# Monumental Verify Your Email Address

You have been registered successfully at monumental.com!

@component('mail::button', ['url' => $user->activation_link])
Click here to activate your account.
@endcomponent

@component('mail::panel')
Email Address : - {{ $user->email }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent