@extends('layouts.admin_login')

@section('title', 'Super Admin Login')

@section('content')
    
	<div class="col-md-5 login-left-bg  text-center">
		<?php $url = asset('public/img/logo-dashboad.png'); ?>
		<img src="{{ $url }}">
	    <div class="call-us">
	        <div class="call-us-heading"></div>
	        <div class="call-phone"><h3>Welcome to Monumental Administration</h3></div>
	    </div>
	    <div class="share-bottom">
	        <div class="follow-text"></div>
	        <ul class="list-inline">
	            <li></li>
	            <li></li>
	            <li></li>
	            <li></li>
	        </ul>
	    </div>
	</div>
	<div class="col-md-7 login-right-bg">
	    <div class="text-center top-right">
	        <div class="user-login">Admin Login</div>
	    </div>
	    {!! Form::open(['url' => '/admin', 'class' => 'login-section']) !!}
	        <div class="form-group">
	            <?php echo Form::label('email', 'Email Address'); ?>
	            <?php
	            	echo Form::text('email', null, [
                        'class' => 'form-control',
                        'placeholder' => __('Email Address'),
                        'autocomplete' => 'on',
                        'label' => false
                    ]);
	            ?>
	        </div>
	        <div class="form-group">
	            <?php echo Form::label('email', 'Password'); ?>
	            <?php
	            	echo Form::password('password', [
                        'class' => 'form-control',
                        'placeholder' => __('Password'),
                         'autocomplete' => 'on'
                    ]);
	            ?>
	        </div>
	        <?php
	        	echo Form::submit('Login', [
	        			'class' => 'btn btn-black btn-block'
	        		]);
	        ?>
	    {!! Form::close() !!}
	</div>

@endsection