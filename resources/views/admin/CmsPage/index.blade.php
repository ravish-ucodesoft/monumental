@extends('layouts.admin')

@section('title', 'Super Admin Cms Pages')

@section('content')
    <section class="content-header clearfix">
        <div class="col-lg-12 heading-top">
            <h1 class="heading-text-color pull-left"><?php echo __('Manage Cms Pages'); ?></h1>
            {!! Form::open(['url' => '/admin/cms-pages', 'method' => 'get', 'novalidate' => true]) !!}
                <div class="input-group my-search col-sm-3 pull-right">
                    <?php
                        echo Form::text('search', '', [
                                'class' => 'form-control',
                                'placeholder' => 'search for team',
                                'label' => false,
                                'required' => true
                            ]);
                    ?>
                    <span class="input-group-btn"> 
                        <?php
                            echo Form::button('Go', [
                                    'class' => 'btn btn-default',
                                    'type' => 'submit'
                                ]);
                        ?>
                     </span>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
    <section class="content shoping-cart clearfix">
        <div class="col-sm-12" id="vue-container">
            <div class="note-listing">
                <div class="table-responsive" id="vue-container">
                    <table class="table shoping-cart-table">
                        <thead>
                            <tr>
                                <th>@sortablelink('page_name', 'Page Name')</th>
                                <th>@sortablelink('meta_title', 'Meta Title')</th>
                                <th>@sortablelink('meta_keywords', 'Meta Keywords')</th>
                                <th>@sortablelink('created_at', 'Created')</th>
                                <th>@sortablelink('updated_at', 'Modified')</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($cmsPages as $cmsPage)
                                <tr>
                                    <td>
                                        {{ $cmsPage->page_name }}
                                    </td>
                                    <td>
                                        {{ $cmsPage->meta_title }}
                                    </td>
                                    <td>
                                        {{ $cmsPage->meta_keywords }}
                                    </td>
                                    <td>
                                        {{ $cmsPage->created_at }}
                                    </td>
                                    <td>
                                        {{ $cmsPage->updated_at }}
                                    </td>
                                    <td>
                                        <a href='{{ asset("/admin/cms-page-edit/$cmsPage->id") }}'><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection