@extends('layouts.admin')

@section('title', 'Super Admin Edit Cms')

@section('content')
   <section class="content-header clearfix">
      <div class="col-lg-12 heading-top">
         <h1 class="heading-text-color pull-left"><?php echo __('Edit CMS Page'); ?></h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box">         
         <div class="col-lg-12">                     
            <div id="emails">
               {!! Form::open(['url' => '/admin/cms-page-update', 'class' => 'form-horizontal', 'novalidate' => true]) !!}
                  <div class="form-group">
                     {{ Form::label('meta_title', 'Meta Title', ['class' => 'control-label required']) }}
                     <?php
                        echo Form::hidden('id', $cmsPage->id);
                        echo Form::text('meta_title', $cmsPage->meta_title, [
                           'class' => 'form-control',
                           'placeholder' => 'Meta Title',
                           'label' => false,
                           'required' => true
                        ]);
                     ?>
                  </div>
                  <div class="form-group">
                    {{ Form::label('meta_keywords', 'Meta Keywords', ['class' => 'control-label required']) }}
                    <?php
                        echo Form::text('meta_keywords', $cmsPage->meta_keywords, [
                           'class' => 'form-control',
                           'placeholder' => 'Meta Keywords',
                           'label' => false,
                           'required' => true
                        ]);
                     ?>
                  </div>
                  <div class="form-group">
                    {{ Form::label('page_title', 'Page Title', ['class' => 'control-label required']) }}
                    <?php
                        echo Form::text('page_title', $cmsPage->page_title, [
                           'class' => 'form-control',
                           'placeholder' => 'Page Title',
                           'label' => false,
                           'required' => true
                        ]);
                     ?>
                  </div>
                  <div class="form-group">
                    {{ Form::label('page_name', 'Page Name', ['class' => 'control-label required']) }}
                    <?php
                        echo Form::text('page_name', $cmsPage->page_name, [
                           'class' => 'form-control',
                           'placeholder' => 'Page Name',
                           'label' => false,
                           'required' => true
                        ]);
                     ?>
                  </div>
                  <div class="form-group">
                    {{ Form::label('meta_description', 'Meta Description', ['class' => 'control-label required']) }}
                    <?php
                        echo Form::textarea('meta_description', $cmsPage->meta_description, [
                           'class' => 'form-control',
                           'placeholder' => 'Meta Description',
                           'label' => false,
                           'required' => true
                        ]);
                     ?>
                  </div>
                  <div class="col-lg-8">
                     <?php
                        echo Form::submit('Submit', [
                           'class' => 'btn submit-info submit_black'
                        ]);
                      ?>
                  </div>
               {!! Form::close() !!}
            </div>
         </div>
      </div>
   </section>
@endsection
@push('extra_scripts')
   <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
   <script>
      tinymce.init({
         selector: "textarea",
         height: 300,
         plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
         ],

         toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
         toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
         toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",
         content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'],

         menubar: false,
         toolbar_items_size: 'small',

         style_formats: [{
            title: 'Bold text',
            inline: 'b',
         }, {
            title: 'Red text',
            inline: 'span',
            styles: {
            color: '#ff0000'
         }
         }, {
            title: 'Red header',
            block: 'h1',
            styles: {
               color: '#ff0000'
            }
         }, {
            title: 'Example 1',
            inline: 'span',
            classes: 'example1'
         }, {
            title: 'Example 2',
            inline: 'span',
            classes: 'example2'
         }, {
            title: 'Table styles'
         }, {
            title: 'Table row 1',
            selector: 'tr',
            classes: 'tablerow1'
         }],

         templates: [{
            title: 'Test template 1',
            content: 'Test 1'
         }, {
            title: 'Test template 2',
            content: 'Test 2'
         }]
      });
   </script>
@endpush