@extends('layouts.admin')

@section('title', 'Super Admin Teams')

@section('content')
    <section class="content-header clearfix">
        <div class="col-lg-12 heading-top">
            <h1 class="heading-text-color pull-left"><?php echo __('Applied Teams'); ?></h1>
            {!! Form::open(['url' => '/admin/teams/applied', 'method' => 'get', 'novalidate' => true]) !!}
                <div class="input-group my-search col-sm-3 pull-right">
                    <?php
                        echo Form::text('search', '', [
                                'class' => 'form-control',
                                'placeholder' => 'search...',
                                'label' => false,
                                'required' => true
                            ]);
                    ?>
                    <span class="input-group-btn"> 
                        <?php
                            echo Form::button('Go', [
                                    'class' => 'btn btn-default',
                                    'type' => 'submit'
                                ]);
                        ?>
                     </span>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
    <section class="content shoping-cart clearfix">
        <div class="col-sm-12" id="vue-container">
            <div class="note-listing">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">
                        <thead>
                            <tr>
                                <th>@sortablelink('team_name', 'Team Name')</th>
                                <th>@sortablelink('full_name', 'User Name')</th>
                                <th>@sortablelink('created_at', 'Created')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($teams as $team)
                                <tr>
                                    <td>
                                        {{ $team->team_name }}
                                    </td>
                                    <td>
                                        {{ $team->full_name }}
                                    </td>
                                    <td>
                                        {{ $team->created_at }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $teams->appends($_GET)->render() !!}
                </div>
            </div>
        </div>
    </section>
@endsection