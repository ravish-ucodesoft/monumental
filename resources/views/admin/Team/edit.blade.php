<script type="text/x-template" id="modal-template-to-edit">
    <div class="modal-mask modal Educational-info" id="edit-team" tabindex="-1" role="dialog">
        <div class="modal-wrapper modal-dialog">
            <div class="modal-container modal-content">

                <div class="modal-header">
                    <a href="#" class="border_radius" @click="$emit('close')">X</a>
                    <h4 class="modal-title" id="myModalLabel"><?= __('Edit Team'); ?></h4>
                </div>

                <div class="modal-body">
                    {!! Form::open(['url' => '/admin/edit-team', 'class' => 'form-horizontal', 'id' => 'admin-edit-team', 'novalidate' => true]) !!}
                        <slot name="editTeamFields">
                        </slot>
                        
                        <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                            <div class="col-lg-8">
                                <?php
                                    echo Form::submit('Submit', [
                                        'class' => 'btn submit-info submit_black'
                                    ]);
                                ?>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</script>