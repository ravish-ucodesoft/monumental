@extends('layouts.admin')

@section('title', 'Super Admin Teams')

@section('content')
    <section class="content-header clearfix">
        <div class="col-lg-12 heading-top">
            <h1 class="heading-text-color pull-left"><?php echo __('Manage Teams'); ?></h1>
            <?php
                echo link_to('javascript::void(0)', 'Import Teams', ['class' => 'btn btn-default add_anchor pull-right', 'data-toggle' => 'modal', 'data-target' => '#add-team']);
                echo link_to('javascript::void(0)', 'Add Team', ['class' => 'btn btn-default add_anchor pull-right', 'data-toggle' => 'modal', 'data-target' => '#add-new-team']);
            ?>
            {!! Form::open(['url' => '/admin/teams', 'method' => 'get', 'novalidate' => true]) !!}
                <div class="input-group my-search col-sm-3 pull-right">
                    <?php
                        echo Form::text('search', '', [
                                'class' => 'form-control',
                                'placeholder' => 'search for team',
                                'label' => false,
                                'required' => true
                            ]);
                    ?>
                    <span class="input-group-btn"> 
                        <?php
                            echo Form::button('Go', [
                                    'class' => 'btn btn-default',
                                    'type' => 'submit'
                                ]);
                        ?>
                     </span>
                </div>
                <div class="input-group my-search my-search-select col-sm-2 pull-right">
                    <?php
                        echo Form::select('category_name', ['' => 'Select a category'] + $categories->toArray(), null, [
                                'class' => 'form-control'
                            ]);
                    ?>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
    <section class="content shoping-cart clearfix">
        <div class="col-sm-12" id="vue-container">
            <div class="note-listing">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">
                        <thead>
                            <tr>
                                <th>@sortablelink('team_name', 'Team Name')</th>
                                <th>@sortablelink('category_name', 'Category')</th>
                                <th>@sortablelink('created_at', 'Created')</th>
                                <th>@sortablelink('updated_at', 'Modified')</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($teams as $team)
                                <tr>
                                    <td>
                                        {{ $team->team_name }}
                                    </td>
                                    <td>
                                        {{ $team->category_name }}
                                    </td>
                                    <td>
                                        {{ $team->created_at }}
                                    </td>
                                    <td>
                                        {{ $team->updated_at }}
                                    </td>
                                    <td>
                                        <span class="switch-toggle custom-switch">
                                            <?php
                                                $statusToChange = ($team->is_active) ? 0 : 1;
                                                $url = url('api/update-status/' . $team->id . '/teams/is_active');
                                                echo Form::checkbox('data', 1, $team->is_active, [
                                                    'id' => 'test' . $team->id,
                                                    'hidden' => true,
                                                    'v-on:change' => "onUpdateStatus('${url}')",
                                                ]);
                                            ?>
                                            <label class="switch" for="<?php echo 'test' . $team->id; ?>"></label>
                                        </span>
                                    </td>
                                    <td>
                                        <?php $editTeamUrl = url('api/teams/' . $team->id); ?>
                                        <a href="#" v-on:click="ediTeam('<?php echo $editTeamUrl; ?>')"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $teams->appends($_GET)->render() !!}
                </div>
            </div>
            <modal v-if="showModal" @close="showModal = false">
                <div slot="editTeamFields" v-cloak>
                    <div class="form-group">
                        <input type="hidden" name="id" class="form-control" v-model="teamId">
                        <label  class="col-sm-4 control-label required"><?php echo __('Team Name'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="team_name" class="form-control" placeholder="<?php echo __('Team name'); ?>" v-model="teamName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label required"><?php echo __('Category Name'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="category_name" class="form-control" placeholder="<?php echo __('Category name'); ?>" v-model="teamCategory">
                            <span class="text-info"><?php echo __('Non existing category name will be created as new category.'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Conference'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="conference" class="form-control" placeholder="<?php echo __('Conference'); ?>" v-model="teamConference">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('State'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="state" class="form-control" placeholder="<?php echo __('State'); ?>" v-model="teamState">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('Region'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="region" class="form-control" placeholder="<?php echo __('Region'); ?>" v-model="teamRegion">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('PPG'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="ppg" class="form-control" placeholder="<?php echo __('PPG Points'); ?>" v-model="teamPPG">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('RPG'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="rpg" class="form-control" placeholder="<?php echo __('RPG Points'); ?>" v-model="teamRPG">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('APG'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="apg" class="form-control" placeholder="<?php echo __('APG Points'); ?>" v-model="teamAPG">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('SPG'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="spg" class="form-control" placeholder="<?php echo __('SPG Points'); ?>" v-model="teamSPG">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('BPG'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="bpg" class="form-control" placeholder="<?php echo __('BPG Points'); ?>" v-model="teamBPG">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('FG%'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="fg" class="form-control" placeholder="<?php echo __('FG%'); ?>" v-model="teamFG">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('FT'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="ft" class="form-control" placeholder="<?php echo __('FT%'); ?>" v-model="teamFT">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('3-PT FG%'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="3_pt_fg" class="form-control" placeholder="<?php echo __('3-PT FG%'); ?>" v-model="team3PTFG">
                        </div>
                    </div>

                </div>
            </modal>
        </div>
    </section>
    @include('admin.Team.add')
    @include('admin.Team.add_new')
    @include('admin.Team.edit')
@endsection