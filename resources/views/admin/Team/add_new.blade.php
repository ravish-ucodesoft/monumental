<div class="modal Educational-info fade" id="add-new-team" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#add-new-team">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center">Add New Team</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/admin/add-new-team', 'class' => 'form-horizontal', 'id' => 'admin-add-team', 'novalidate' => true, 'files' => true]) !!}
                    <div class="form-group">
                        {{ Form::label('team_name', 'Team Name', ['class' => 'col-sm-4 control-label required']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('team_name', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Team Name',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('category_name', 'Category Name', ['class' => 'col-sm-4 control-label required']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('category_name', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Category Name',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                            <span class="text-info"><?php echo __('Non existing category name will be created as new category.'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('conference', 'Conference', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('conference', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Conference',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('state', 'State', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('state', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'State',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('region', 'Region', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('region', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Region',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('ppg', 'PPG', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('ppg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'PPG',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('rpg', 'RPG', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('rpg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'RPG',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('apg', 'APG', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('apg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'APG',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('spg', 'SPG', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('spg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'SPG',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('bpg', 'BPG', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('bpg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'BPG',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('fg', 'FG%', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('fg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'FG',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('ft', 'FT%', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('ft', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'FT',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('3_pt_fg', '3-PT FG%', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('3_pt_fg', null, [
                                    'class' => 'form-control',
                                    'placeholder' => '3-PT FG%',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo Form::submit('Submit', [
                                    'class' => 'btn submit-info submit_black'
                                ]);
                            ?>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>