@extends('layouts.admin')

@section('title', 'Super Admin Dashboard')

@section('content')
	<section class="content-header clearfix">
		<div class="col-lg-12 heading-top">
			<h1 class="heading-text-color pull-left">Dashboard</h1>
		</div>
	</section>
	<section class="dash_board">
		<div class="container-fluid">
			<div class="dashboard-top clearfix">
				<div class="col-xs-12 col-sm-6 col-lg-3 boxes-margin-bottom">
			        <div class="outer-box-red clearfix">
			            <div class="col-xs-8 red-box">
			                <b>{{ $totalUsers }}</b>
			                <div>Total</div>
			                Players Registered
			            </div>
			            <div class="col-xs-4 text-center">
			                <i class="fa fa-users" aria-hidden="true"></i>
			            </div>
			        </div>
			    </div>
			    <div class="col-xs-12 col-sm-6 col-lg-3 boxes-margin-bottom">
			        <div class="outer-box-blue clearfix">
			            <div class="col-xs-8 blue-box">
			                <b>{{ $totalTeams }}</b>
			                <div><?php echo __('Total'); ?></div>
			                Colleges/Teams
			            </div>
			            <div class="col-xs-4 text-center">
			                <i class="fa fa-cubes" aria-hidden="true"></i>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</section>
@endsection

@push('extrastyles')
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/Administration/dashboard.css') }}">
@endpush