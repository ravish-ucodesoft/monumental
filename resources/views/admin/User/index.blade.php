@extends('layouts.admin')

@section('title', 'Super Admin Players')

@section('content')
    <section class="content-header clearfix">
        <div class="col-lg-12 heading-top">
            <h1 class="heading-text-color pull-left"><?php echo __('Manage Players'); ?></h1>
            {!! Form::open(['url' => '/admin/players', 'method' => 'get', 'novalidate' => true]) !!}
                <div class="input-group my-search col-sm-4 pull-right">
                    <?php
                        echo Form::text('search', '', [
                                'class' => 'form-control',
                                'placeholder' => 'search for first name, last name, email or phone',
                                'label' => false,
                                'required' => true
                            ]);
                    ?>
                    <span class="input-group-btn"> 
                        <?php
                            echo Form::button('Go', [
                                    'class' => 'btn btn-default',
                                    'type' => 'submit'
                                ]);
                        ?>
                     </span>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
    <section class="content shoping-cart clearfix">
        <div class="col-sm-12" id="vue-container">
            <div class="note-listing">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">
                        <thead>
                            <tr>
                                <th>@sortablelink('first_name', 'First Name')</th>
                                <th>@sortablelink('last_name', 'Last Name')</th>
                                <th>@sortablelink('role', 'Role')</th>
                                <th>@sortablelink('email', 'Email')</th>
                                <th>@sortablelink('phone', 'Phone')</th>
                                <th>@sortablelink('date_of_birth', 'Date Of Birth')</th>
                                <th>@sortablelink('gender', 'Gender')</th>
                                <th>@sortablelink('zipcode', 'Zipcode')</th>
                                <th>@sortablelink('Country', 'country')</th>
                                <th>@sortablelink('created_at', 'Created')</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                                <tr>
                                    <td>
                                        {{ $user->first_name }}
                                    </td>
                                    <td>
                                        {{ $user->last_name }}
                                    </td>
                                    <td>
                                        {{ config('app.UserTypes.' . $user->user_type ) }}
                                    </td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        {{ $user->phone }}
                                    </td>
                                    <td>
                                        {{ $user->date_of_birth }}
                                    </td>
                                    <td>
                                        {{ $user->gender }}
                                    </td>
                                    <td>
                                        {{ $user->zipcode }}
                                    </td>
                                    <td>
                                        {{ Cache('countries')[$user->country] }}
                                    </td>
                                    <td>
                                        {{ $user->created_at }}
                                    </td>
                                    <td>
                                        <span class="switch-toggle custom-switch">
                                            <?php
                                                $statusToChange = ($user->active) ? 0 : 1;
                                                $url = url('api/update-status/' . $user->id . '/users/active');
                                                echo Form::checkbox('data', 1, $user->active, [
                                                    'id' => 'test' . $user->id,
                                                    'hidden' => true,
                                                    'v-on:change' => "onUpdateStatus('${url}')",
                                                ]);
                                            ?>
                                            <label class="switch" for="<?php echo 'test' . $user->id; ?>"></label>
                                        </span>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="13" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $users->appends($_GET)->render() !!}
                </div>
            </div>
        </div>
    </section>
@endsection