<div class="modal Educational-info fade" id="edit-category" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#edit-category">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center">Edit Category</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/admin/edit-category', 'class' => 'form-horizontal', 'id' => 'admin-add-cateogry', 'novalidate' => true]) !!}
                    <div class="form-group">
                        {{ Form::label('category name', 'Category Name', ['class' => 'col-sm-4 control-label required']) }}
                        {{ Form::hidden('id') }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('name', null, [
                                    'class' => 'form-control edit-category-value',
                                    'placeholder' => 'Categrory Name',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo Form::submit('Submit', [
                                    'class' => 'btn submit-info submit_black'
                                ]);
                            ?>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>