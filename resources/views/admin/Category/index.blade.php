@extends('layouts.admin')

@section('title', 'Super Admin Dashboard')

@section('content')
    <section class="content-header clearfix">
        <div class="col-lg-12 heading-top">
            <h1 class="heading-text-color pull-left"><?php echo __('Manage Categories'); ?></h1>
            <?php
                echo link_to('javascript::void(0)', 'Add Category', ['class' => 'btn btn-default add_anchor pull-right', 'data-toggle' => 'modal', 'data-target' => '#add-category']);
            ?>
            {!! Form::open(['url' => '/admin/categories', 'method' => 'get', 'novalidate' => true]) !!}
                <div class="input-group my-search col-sm-3 pull-right">
                    <?php
                        echo Form::text('search', '', [
                                'class' => 'form-control',
                                'placeholder' => 'search for category',
                                'label' => false,
                                'required' => true
                            ]);
                    ?>
                    <span class="input-group-btn"> 
                        <?php
                            echo Form::button('Go', [
                                    'class' => 'btn btn-default',
                                    'type' => 'submit'
                                ]);
                        ?>
                     </span>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
    <section class="content shoping-cart clearfix">
        <div class="col-sm-12">
            <div class="note-listing">
                <div class="table-responsive">
                    <table class="table shoping-cart-table" id="list-table">
                        <thead>
                            <tr>
                                <th>@sortablelink('name', 'Name')</th>
                                <th>@sortablelink('created_at', 'Created')</th>
                                <th>@sortablelink('updated_at', 'Modified')</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($categories as $category)
                                <tr>
                                    <td>
                                        {{ $category->name }}
                                    </td>
                                    <td>
                                        {{ $category->created_at }}
                                    </td>
                                    <td>
                                        {{ $category->updated_at }}
                                    </td>
                                    <td>
                                        <span class="switch-toggle custom-switch">
                                            <?php
                                                $statusToChange = ($category->is_active) ? 0 : 1;
                                                $url = url('api/update-status/' . $category->id . '/categories/' . $statusToChange);
                                                echo Form::checkbox('data', 1, $category->is_active, [
                                                    'id' => 'test' . $category->id,
                                                    'class' => 'update-status-switch',
                                                    'hidden' => true,
                                                    'v-on:change' => "onUpdateStatus('${url}')"
                                                ]);
                                            ?>
                                            <label class="switch" for="<?php echo 'test' . $category->id; ?>"></label>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="/api/categories/<?php echo $category->id; ?>" class="edit-category"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $categories->render() !!}
                </div>
            </div>
        </div>
    </section>
    @include('admin.Category.add')
    @include('admin.Category.edit')
@endsection
@push('extra_scripts')
    <script src="/js/Administration/Category/index.js"></script>
    <script src="/js/Administration/Category/add_category.js"></script>
@endpush