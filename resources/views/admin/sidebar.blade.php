<!-- Sidebar Left-->
<aside class="main-sidebar" id="left-vue">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview" v-bind:class="{active : (currentRoute == '/admin/dashboard')}">
                <a href="{{ asset('/admin/dashboard') }}"><i class="fa fa-dashboard" aria-hidden="true"></i> <span> Dashboard</a>
            </li>
            {{-- <li class="treeview" v-bind:class="{active : (currentRoute == '/admin/categories')}">
                <a href="/admin/categories"><i class="fa fa-tags" aria-hidden="true"></i> <span> Manage Categories</a>
            </li> --}}
            <li class="treeview" v-bind:class="{active : (currentRoute == '/admin/players')}">
                <a href="{{ asset('/admin/players') }}"><i class="fa fa-users" aria-hidden="true"></i> <span> Manage Players</a>
            </li>
            <li class="treeview" v-bind:class="{active : (currentRoute == '/admin/teams')}">
                <a href="{{ asset('/admin/teams') }}"><i class="fa fa-university" aria-hidden="true"></i> <span> Manage Teams</a>
            </li>
            <li class="treeview" v-bind:class="{active : (currentRoute == '/admin/teams/applied')}">
                <a href="{{ asset('/admin/teams/applied') }}"><i class="fa fa-check-circle" aria-hidden="true"></i> <span> Users Applied Teams</a>
            </li>
            <li class="treeview" v-bind:class="{active : (currentRoute == '/admin/cms-pages')}">
                <a href="{{ asset('/admin/cms-pages') }}"><i class="fa fa-book" aria-hidden="true"></i> <span> Manage Cms Pages</a>
            </li>
        </ul>
    </section>
</aside>