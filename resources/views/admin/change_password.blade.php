<div class="modal Educational-info fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#changePasswordModal">X</a>
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Change Password</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/admin/change-password', 'id' => 'admin-change-password']) !!}
                    <div class="form-group">
                        {{ Form::label('old_password', 'Old Password', ['class' => 'control-label required']) }}
                        <?php
                            echo Form::password('old_password', [
                                'class' => 'form-control',
                                'placeholder' => 'Old Password',
                                'label' => false
                            ]);
                        ?>
                    </div>
                    <div class="form-group">
                        {{ Form::label('password', 'Password', ['class' => 'control-label required']) }}
                        <?php
                            echo Form::password('password', [
                                'class' => 'form-control',
                                'placeholder' => 'New Password',
                                'label' => false,
                                'id' => 'new-admin-password'
                            ]);
                        ?>
                    </div>
                    <div class="form-group">
                        {{ Form::label('password_confirmation', 'Confirm Password', ['class' => 'control-label required']) }}
                        <?php
                            echo Form::password('password_confirmation', [
                                'class' => 'form-control',
                                'placeholder' => 'Confirm Password',
                                'label' => false
                            ]);
                        ?>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo Form::submit('Submit', [
                                    'class' => 'btn submit-info submit_black'
                                ]);
                            ?>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@push('extra_scripts')
    <script src="/js/Administration/change_password.js"></script>
@endpush