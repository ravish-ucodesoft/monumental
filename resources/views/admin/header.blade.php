<header class="main-header">
    <!-- Logo -->
    <?php
        $mainLogoUrl = asset('public/img/logo-dashboad.png');
        $miniLogoUrl = asset('public/img/logo-dashboad.png');
    ?>
    <a href="/admin/dashboard" class="logo">
        <span class="logo-mini"><img src="{{ $miniLogoUrl }}"></span>
        <span class="logo-lg"><img src="{{ $mainLogoUrl }}"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top nav-custom">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><i class="fa fa-bars fa-1" aria-hidden="true"></i></a>
        <div class="col-sm-6  col-md-5 col-lg-4 col-lg-offset-3 search-top">
            <div class="right-inner-addon">
                <h2>Super Admin Panel</h2>
            </div>
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">            
                <li class="dropdown user user-menu">
                    <?php $profileImg = asset('public/img/images.jpeg'); ?>
                    @if (File::exists(public_path().'/uploads/images/1/' . Auth::user()->profile_pic))
                        <?php $profileImg = asset('public/uploads/images/' . Auth::id() . '/' . Auth::user()->profile_pic); ?>
                    @endif
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ $profileImg }}" class="user-image"><span class="hidden-xs hidden-sm">Admin Admin</span>
                    </a>
                    <ul aria-labelledby="drop3" class="dropdown-menu">
                        <li>
                            <?php
                                echo link_to('javascript:void(0)', 'Profile', [
                                        'class' => 'editprofile',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#updateProfile',
                                    ]);
                            ?>
                        </li>
                        <li>
                            <?php
                                echo link_to('javascript:void(0)', 'Change Password', [
                                        'data-toggle' => 'modal',
                                        'data-target' => '#changePasswordModal',
                                    ]);
                            ?>
                        </li>
                        <li>
                            <a href="{{ asset('/admin/logout') }}">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>