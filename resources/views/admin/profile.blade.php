<div class="modal Educational-info fade" id="updateProfile" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#updateProfile">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Update Profile'); ?></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/admin/update-profile', 'class' => 'form-horizontal', 'id' => 'admin-profile-update', 'files' => true]) !!}
                    <div class="form-group">
                        {{ Form::label('full_name', 'Full Name', ['class' => 'col-sm-4 control-label required']) }}
                        
                        <div class="col-sm-4">
                            <?php
                                echo Form::text('first_name', Auth::user()->first_name, [
                                    'class' => 'form-control',
                                    'placeholder' => 'First Name',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo Form::text('last_name', Auth::user()->last_name, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Last Name',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', 'Phone Number', ['class' => 'col-sm-4 control-label required']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::text('phone', Auth::user()->phone, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Phone number',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('address', 'Address', ['class' => 'col-sm-4 control-label required']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::textarea('address', Auth::user()->address, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Address',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Email', ['class' => 'col-sm-4 control-label required']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::email('email', Auth::user()->email, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Email',
                                    'label' => false,
                                    'required' => true
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('profile_pic', 'Profile Picture', ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-8">
                            <?php
                                echo Form::file('profile_pic', [
                                    'class' => 'form-control',
                                    'label' => false
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo Form::submit('Submit', [
                                    'class' => 'btn submit-info submit_black'
                                ]);
                            ?>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@push('extra_scripts')
    <script src="/js/Administration/edit_profile.js"></script>
@endpush