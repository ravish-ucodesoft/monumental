<footer class="footer">
   <div class="container">
      <div class="col-sm-6">
         <p class="text-muted">©2017 by Monumental Athletics Management.</p>
      </div>
      <div class="col-sm-6">
         <ul class="list-unstyled list-inline text-right social-media">
            <li>242-804-0621</li>
            <li>
               <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
            </li>
         </ul>
      </div>
   </div>
</footer>