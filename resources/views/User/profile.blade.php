@extends('layouts.pages')

@section('title', 'Profile')

@section('content')
   <div class="container-fluid margin-top-fixed">
      <div class="container">
         <div class="row">
            <div style="padding-top:50px;"> </div>
            <div class=" col-sm-12 ">
               @include('flash::message')
               <div class="panel panel-default">
                  <div class="panel-body">
                     <h1 class="panel-title pull-left" style="font-size:30px;">Profile</h1>
                         <!--<a href="" class="btn btn-orange pull-right">Save</a>-->
                  </div>
               </div>
               <div class="panel panel-default">
                  <div class="panel-body">
                     <h3 class="panel-title pull-left">Personal Inoformation</h3>
                     <br>
     					  	<div class="col-lg-6 margin-top-default">
  							   <label for="First_name">Name</label>
                        {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}
                    </div>
                     <div class="col-lg-6 margin-top-default">
  							   <label for="email">Email Address</label>
                        {{ Auth::user()->email }}
                     </div>
                     <div class="col-lg-6 margin-top-default">
                        <label for="phone">Phone Number</label>
                        {{ Auth::user()->phone }}
                     </div>
                     <div class="col-lg-6 margin-top-default">
                        <label for="Your_gender">Your gender</label>
                        {{ Auth::user()->gender }}
                     </div>
                     <br>
                     <div class="col-lg-6 margin-top-default">
                        <label>Your Birthday</label>
                        <div class="form-inline" id="birth-date">
                           {{-- 12 jan 2000 --}}
                           {{ Auth::user()->date_of_birth }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="panel panel-default">
                  <div class="panel-body">
                     <h3 class="panel-title pull-left">Contact Information</h3>
                     <br>
                     <div class="col-lg-6 margin-top-default">
                        <label for="Zip Code">Zip Code</label>
                        {{ Auth::user()->zipcode }}
                     </div>
                     <div class="col-lg-6 margin-top-default">
                        <label for="country">Country</label>
                        {{ Cache('countries')[Auth::user()->country] }}
                     </div>
                     <br>
                  </div>
               </div>
               <hr>
               <div class="panel panel-default">
     				   <div class="panel-body">
     					  <a href="{{ asset('/edit-profile/' . Auth::user()->id) }}" class="btn btn-orange"><i class="fa fa-fw fa-check" aria-hidden="true"></i> Edit Profile</a>
     				   </div>
               </div>
            </div>
         </div>
      </div>
   </div>
@endsection