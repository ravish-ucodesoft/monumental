@extends('layouts.pages')

@section('title', 'Edit Profile')

@section('content')
    <div class="container-fluid margin-top-fixed">
        <div class="container">
            <div class="row">
                <div style="padding-top:50px;"> </div>
                <div class=" col-sm-12 ">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>        
                            @endforeach
                        </div>
                    @endif
                    @include('flash::message')
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h1 class="panel-title pull-left" style="font-size:30px;">Edit profile</h1>
                            <!--<a href="" class="btn btn-orange pull-right">Save</a>-->
                        </div>
                    </div>
                    {!! Form::model($user, ['url' => ['user-update', $user->id], 'class' => 'form-horizontal', 'novalidate' => true]) !!}
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 class="panel-title pull-left">Personal Inoformation</h3>
                                <br>
            						<div class="col-lg-6 margin-top-default">
            							{{ Form::label('first_name', 'First Name', ['class' => 'required']) }}
                                        <?php
                                            echo Form::text('first_name', null, [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'required' => true
                                            ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-6 margin-top-default">
            							{{ Form::label('last_name', 'Last Name', ['class' => 'required']) }}
                                        <?php
                                            echo Form::text('last_name', null, [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'required' => true
                                            ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-6 margin-top-default">
            							{{ Form::label('email_address', 'Email Address', ['class' => 'required']) }}
                                        <?php
                                            echo Form::text('email', null, [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'required' => true
                                            ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-6 margin-top-default">
                                        {{ Form::label('phone_number', 'Phone Number') }}
                                        <?php
                                            echo Form::text('phone', null, [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'required' => true
                                            ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-6 margin-top-default">
                                        {{ Form::label('gender', 'Gender', ['class' => 'required']) }}
                                        <?php
                                            echo Form::select('gender', ['' => 'Select Your Gender'] + config('app.Gender'), null,
                                            [
                                                'class' => 'form-control'
                                            ]);
                                        ?>
            						</div>
                                    <br>
                                    <div class="col-lg-6 margin-top-default">
                                        {{ Form::label('date of birth', 'Date of Birth', ['class' => 'required']) }}
                                        <?php echo Form::date('date_of_birth', \Carbon\Carbon::createFromFormat('Y-m-d', $user->date_of_birth), [
                                                    'class' => 'form-control'
                                            ]);
                                        ?>
            						</div>
                                    <div class="col-lg-6 margin-top-default">
                                        {{ Form::label('zip code', 'Zip Code') }}
                                        <?php
                                            echo Form::text('zipcode', null, [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'required' => true
                                            ]);
                                        ?>
                                    </div>
                                    <br>
                                    <div class="col-lg-6 margin-top-default">
                                        {{ Form::label('country', 'Country', ['class' => 'required']) }}
                                        <?php
                                            echo Form::select('country', Cache('countries'), NULL,
                                            [
                                                'class' => 'form-control'
                                            ]);
                                        ?>
                                    </div>
                            </div>
                        </div>
                    
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="{{ asset('/profile') }}" class="btn btn-orange"><i class="fa fa-fw fa-times" aria-hidden="true"></i> Cancel</a>
                                {{
                                    Form::button('<i class="fa fa-fw fa-check" aria-hidden="true"></i> Update Profile', [
                                       'class' => 'btn btn-orange',
                                       'type' => 'submit'
                                    ])
                                }}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection