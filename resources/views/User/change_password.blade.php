@extends('layouts.pages')

@section('title', 'Change Password')

@section('content')
    <div class="container sign-in-section">
        @include('flash::message')
        {{-- {{ dd($errors) }} --}}
        <div class="col-sm-5 col-sm-offset-3 signup-box">
            {!! Form::model(['url' => ['change-password'], 'class' => 'form-horizontal', 'novalidate' => true]) !!}
                <h2 class="text-center">Change Password</h2>
                <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                    {{ Form::label('old_password', 'Old Password', ['class' => 'required']) }}
                    {{
                        Form::password('old_password', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('old_password') }}</p>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::label('password', 'Password', ['class' => 'required']) }}
                    {{
                        Form::password('password', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('password') }}</p>
                </div>
                <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                    {{ Form::label('confirm_password', 'Confirm Password', ['class' => 'required']) }}
                    {{
                        Form::password('password_confirmation', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('password_confirmation') }}</p>
                </div>
                <div class="form-group">
                    {{
                        Form::button('Change Password', [
                            'class' => 'btn btn-orange btn-block',
                            'type' => 'submit'
                        ])
                    }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection