@extends('layouts.user')

@section('title', 'Teams')

@section('content')
    <div class="container team-wraper">
        <div class="header-top clearfix">
            <div class="col-sm-4">
                <h1>Teams</h1>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-5">
                    <label>Division</label>
                    <select class="form-control input-lg">
                        <option> NCAA DI</option>
                        <option>  NJCAA</option>
                    </select>
                </div>
                <div class="col-sm-5">
                    <label>Stats</label>
                    <select class="form-control input-lg">
                        <option>PPG</option>
                        <option>RPG</option>
                        <option>APG</option>
                        <option>SPG</option>
                        <option>BPG</option>
                        <option>FG%</option>
                        <option>FT%</option>
                        <option>3-PT FG%</option>
                    </select>
                </div>
                <div class="col-sm-2"><button class="btn btn-orange btn-lg">Search</button></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Team Name</th>
                            <th>Division</th>
                            <th>PPG</th>
                            <th>RPG</th>
                            <th>APG</th>
                            <th>SPG</th>
                            <th>BPG</th>
                            <th>FG%</th>
                            <th>FT%</th>
                            <th>3-PT FG%</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Abilene Christian University Wildcats</td>
                            <td>NCAA DI</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Abilene Christian University Wildcats</td>
                            <td>NCAA DI</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Abilene Christian University Wildcats</td>
                            <td>NCAA DI</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Abilene Christian University Wildcats</td>
                            <td>NCAA DI</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Abilene Christian University Wildcats</td>
                            <td>NCAA DI</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                            <td>464</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection;