@extends('layouts.pages')

@section('title', 'Forgot Password')

@section('content')
    <div class="container sign-in-section">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        {!! Form::open(['route' => ['password.email'], 'class' => 'form-horizontal', 'novalidate' => true]) !!}
            {{ csrf_field() }}
            <div class="col-sm-5 col-sm-offset-3 signup-box">
                <h2 class="text-center">Forgot Password</h2>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::label('email_address', 'Email Address', ['class' => 'required']) }}
                    {{
                        Form::email('email', old('email'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {{
                        Form::button('Send Now', [
                            'class' => 'btn btn-orange btn-block',
                            'type' => 'submit'
                        ])
                    }}
                </div>
                <div class="form-group text-center"> <a href="{{ asset('sign-in') }}"> Sign In</a> </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection