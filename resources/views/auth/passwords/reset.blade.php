@extends('layouts.pages')

@section('title', 'Reset Password')

@section('content')
    <div class="container sign-in-section">
        @include('flash::message')
        {{-- {{ dd($errors) }} --}}
        <div class="col-sm-5 col-sm-offset-3 signup-box">
            <h2 class="text-center">Reset Password</h2>
            {!! Form::open(['route' => ['password.request'], 'class' => 'form-horizontal', 'novalidate' => true]) !!}
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">


                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::label('email', 'E-Mail Address', ['class' => 'required']) }}
                    {{
                        Form::email('email', $email or old('email'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                            'autofocus' => true
                        ])
                    }}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::label('password', 'Password', ['class' => 'required']) }}
                    {{
                        Form::password('password', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    {{ Form::label('confirm_password', 'Confirm Password', ['class' => 'required']) }}
                    {{
                        Form::password('password_confirmation', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif

                </div>
                <div class="form-group">
                    {{
                        Form::button('Change Password', [
                            'class' => 'btn btn-orange btn-block',
                            'type' => 'submit'
                        ])
                    }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection