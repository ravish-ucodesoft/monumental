@extends('layouts.pages')

@section('title', $pageData->page_name)

@section('content')
   
   <div class="our-motivation margin-top-fixed">
      <div class="container">
         <h1>{{ $pageData->page_name }} </h1>
         <p>{!! $pageData->meta_description !!}</p>
      </div>
   </div>
@endsection