<div class="navbar-fixed-top">
   <nav class="navbar navbar-default nav-top  clearfix ">
      <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="{{ asset('/') }}">{{ Html::image(asset('public/img/logo-dashboad.png'), "logo", ['width' => '79px']) }}</a>
         </div>
         <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right ">
                  <li class=""> <a href="{{ asset('/teams/list') }}">Teams</a> </li>
                  <li class=""> <a href="{{ asset('/teams/index') }}">Scout For Teams</a> </li>
                  <li class="dropdown">
                     <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ Auth::user()->first_name }} <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="{{ asset('/profile') }}">Edit Profile</a></li>
                        <li><a href="{{ asset('/change-password') }}">Change password</a></li>
                        <li><a href="{{ asset('/logout') }}">Logout</a></li>
                     </ul>
                  </li>
            </ul>
         </div>
      </div>
   </nav>
</div>