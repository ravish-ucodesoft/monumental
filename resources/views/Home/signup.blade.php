@extends('layouts.pages')

@section('title', 'Sign Up')

@section('content')
    <div class="container home">
        @include('flash::message')
        <div class="col-sm-6 col-sm-offset-3 signup-box">
            <h2 class="text-center">Sign Up</h2>
            {!! Form::open(['url' => '/register', 'class' => 'form-horizontal', 'novalidate' => true]) !!}
                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    {{ Form::label('first name', 'First Name', ['class' => 'required']) }}
                    {{
                        Form::text('first_name', old('first_name'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true,
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('first_name') }}</p>
                </div>
                 <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    {{ Form::label('last name', 'Last Name', ['class' => 'required']) }}
                    {{
                        Form::text('last_name', old('last_name'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('last_name') }}</p>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::label('email address', 'Email Address', ['class' => 'required']) }}
                    {{
                        Form::text('email', old('email'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('email') }}</p>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::label('password', 'Password', ['class' => 'required']) }}
                    {{
                        Form::password('password', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('password') }}</p>
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    {{ Form::label('confirm password', 'Confirm Password', ['class' => 'required']) }}
                    {{
                        Form::password('password_confirmation', [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('password_confirmation') }}</p>
                </div>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    {{ Form::label('phone number', 'Phone Number') }}
                    {{
                        Form::text('phone', old('phone'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('phone') }}</p>
                </div>
                <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                    {{ Form::label('date of birth', 'Date of Birth', ['class' => 'required']) }}
                    {{
                        Form::date('date_of_birth', \Carbon\Carbon::createFromFormat('Y-m-d', '0000-00-00'), [
                                'class' => 'form-control'
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('date_of_birth') }}</p>
                </div>
        		<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
        			{{ Form::label('gender', 'Gender', ['class' => 'required']) }}
                    {{
                        Form::select('gender', ['' => 'Select Your Gender'] + config('app.Gender'), old('gender'),
                        [
                            'class' => 'form-control'
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('gender') }}</p>
        		</div>
    			<div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
    				{{ Form::label('zip code', 'Zip Code') }}
                    {{
                        Form::text('zipcode', old('zipcode'), [
                            'class' => 'form-control',
                            'label' => false,
                            'required' => true
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('zipcode') }}</p>
                </div>
                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
        			{{ Form::label('country', 'Country', ['class' => 'required']) }}
    				{{
                        Form::select('country', Cache('countries'), 'US',
                        [
                            'class' => 'form-control'
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('country') }}</p>
        		</div>		
                <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
        			{{ Form::label('role', 'Role', ['class' => 'required']) }}
    				{{
                        Form::select('user_type', ['' => 'Select Role'] + config('app.UserTypes'), old('user_type'),
                        [
                            'class' => 'form-control'
                        ])
                    }}
                    <p class="error-field">{{ $errors->first('user_type') }}</p>
        		</div>
        		<div class="form-group">
        			<label class="checkbox-inline">
                        {{ Form::checkbox('agree', 1) }} you are agreeing to the <a href="{{ asset('/terms') }}" target="_blank">Terms</a> of Use and the <a href="{{ asset('/privacy-policy') }}" target="_blank">Privacy Policy</a>.
                    </label>
                    <p class="error-field">{{ $errors->first('agree') }}</p>
        		</div>
                <div class="form-group">
                    <?php
                        echo Form::submit('Create an Account', [
                           'class' => 'btn btn-orange btn-block'
                        ]);
                    ?>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection;