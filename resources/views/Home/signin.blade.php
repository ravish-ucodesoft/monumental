@extends('layouts.pages')

@section('title', 'Sign In')

@section('content')
   <div class="container home">
      @include('flash::message')
      @if ($errors->any())
          <div class="alert alert-danger">
              @foreach ($errors->all() as $error)
                  {{ $error }}<br>        
              @endforeach
          </div>
      @endif
      <div class="col-sm-5 col-sm-offset-3 signup-box">
         <h2 class="text-center">Sign In</h2>
         {!! Form::open(['route' => 'login', 'class' => 'form-horizontal', 'novalidate' => true]) !!}
            <div class="form-group">
               {{ Form::label('email', 'Email', ['class' => 'required']) }}
               <?php
                  echo Form::text('email', null, [
                      'class' => 'form-control',
                      'label' => false,
                      'required' => true,
                      'placeholder' => 'Enter Your Email'
                  ]);
              ?>
            </div>
            <div class="form-group">
               {{ Form::label('password', 'Password', ['class' => 'required']) }}
               <?php
                  echo Form::password('password', [
                      'class' => 'form-control',
                      'label' => false,
                      'required' => true,
                      'placeholder' => 'Enter Your Password'
                  ]);
              ?>
            </div>
            <div class="form-group">
               <?php
                  echo Form::submit('Sign In', [
                     'class' => 'btn btn-orange btn-block'
                  ]);
              ?>
            </div>
            <div class="form-group text-center"> <a href="{{ asset('/password/reset') }}"> Forgot Password?</a> </div>
            <div class="signin-border-bottom form-group"></div>
            <div class="form-group"> <a class="btn btn-orange-border btn-block" href="{{ asset('/sign-up') }}">Create an Account</a></div>
         {!! Form::close() !!}
      </div>
   </div>
@endsection;