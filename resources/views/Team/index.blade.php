@extends('layouts.user')

@section('title', 'Teams')

@section('content')
   <div class="container-fluid margin-top-fixed clearfix">
      <div class="bg-search container margin-top-fixed">
         {!! Form::open(['url' => '/teams/index', 'class' => 'form-horizontal', 'novalidate' => true, 'method' => 'get']) !!}
            @include('flash::message')
            <h2 class="text-center">Search listings </h2>
            <div class="col-xs-12">
               <div class="form-group">
                  <div class="searchable-container">
                     <label>Select Multiple Categories</label>
                     <div class="row">
                        @foreach ($categories as $key => $value)
                           <div class="items  col-sm-6 col-md-3 col-lg-2">
                              <div class="info-block block-info clearfix">
                                 <div class="square-box pull-left"> </div>
                                 <div data-toggle="buttons" class="btn-group bizmoduleselect">
                                    <?php $isActive = (!empty(request('category')) && in_array($value, request('category'))) ? true : false; ?>
                                    <label class="btn btn-default selectiable {{ ($isActive) ? 'active' : '' }}">
                                       <div class="bizcontent">
                                          {!! Form::checkbox('category[]', $value, false, ['class' => 'checkbox-cate']) !!}
                                          <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
                                          <label class="categorie-select">{{ $key }}</label>
                                       </div>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        @endforeach
                    </div>
                  </div>
               </div>
               <div class="form-group  score-field ">
                  <label>Your Score</label>
                  <div class="row">
                     <div class="col-sm-2">
                        {{ Form::label('point per game', 'Point Per Game', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('ppg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('rebounds per game', 'Rebounds Per Game', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('rpg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('assists per game', 'Assists Per Game', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('apg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('steal per game', 'Steal Per Game', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('spg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('bock per game', 'Block Per Game', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('bpg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('fg%', 'FG%', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('fg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('ft%', 'FT%', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('ft', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                     <div class="col-sm-2">
                        {{ Form::label('3 pt ft%', '3-PT%', ['class' => 'inner-label']) }}
                        <?php
                           echo Form::text('three_pt_fg', null, [
                               'class' => 'form-control',
                               'label' => false,
                               'required' => true
                           ]);
                        ?>
                     </div>
                  		<div class="col-sm-2  pull-right">
                        {{ Form::button('Find Now', ['class' => 'btn btn-orange btn-block', 'type' => 'submit']) }}
                     </div>
                  </div>
               </div>
            </div>
         {!! Form::close() !!}
      </div>
   </div>
   <div class="container bg-search">
      <div class="col-md-12" id="vue-teams">
         {!! Form::open(['url' => '/teams/apply', 'class' => 'form-horizontal']) !!}
            <div class="clearfix">
                  <div class="pull-left">
                     <h2>Teams</h2>
                     <p>Click on the collapsible panel to open and close it.</p>
                  </div>
                  @if (!empty($teams))
                     <div class="pull-right">
                        {{ Form::button('Apply', ['class' => 'btn btn-block-hope btn-orange', 'type' => 'submit']) }}
                     </div>
                  @endif
            </div>
            <?php $i = 0; ?>
            @if (!empty($teams))
               @foreach ($teams as $category => $teams)

                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title"> <a data-toggle="collapse" href="#{{ snake_case($category) }}">{{ $category }} </a> </h4>
                        </div>
                        <div id="{{ snake_case($category) }}" class="panel-collapse collapse in">
                           <div class="panel-body">
                              <div class="table-container table-responsive">
                                 <table class="table table-filter ">
                                    <thead>
                                       <tr>
                                          <th width="90">All
                                             <div class="ckbox">
                                                {!! Form::checkbox('teams_all[]', null, false, ['id' => 'parent' . $i, 'v-on:click' => 'markChecked();']) !!}
                                                <label for="{{ 'parent'.$i }}"></label>
                                             </div>
                                          </th>
                                          <th width="300">Team</th>
                                          <th>Conference</th>
                                          <th>State</th>
                                          <th>PPG</th>
                                          <th>RPG</th>
                                          <th>APG</th>
                                          <th>SPG</th>
                                          <th>BPG</th>
                                          <th>FG%</th>
                                          <th>FT%</th>
                                          <th width="80">3-PT% </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       @foreach ($teams as $team)
                                          <tr data-status="pagado">
                                             <td>
                                                @if (!in_array($team->id, $appliedTeams))
                                                   <div class="ckbox">
                                                      {!! Form::checkbox('user_applied_teams[]', $team->id, false, ['id' => $team->id, 'v-on:click' => 'markParentChecked();']) !!}
                                                      <label for="{{ $team->id }}"></label>
                                                   </div>
                                                @else
                                                   Applied
                                                @endif
                                             </td>
                                             <td> {{ $team->team_name }} </td>
                                             <td> {{ $team->conference }} </td>
                                             <td> {{ $team->state }} </td>
                                             <td> {{ $team->ppg }} </td>
                                             <td> {{ $team->rpg }} </td>
                                             <td> {{ $team->apg }} </td>
                                             <td> {{ $team->spg }} </td>
                                             <td> {{ $team->bpg }} </td>
                                             <td> {{ $team->fg }} </td>
                                             <td> {{ $team->ft }} </td>
                                             <td width="80"> {{ $team->three_pt_fg }} </td>
                                          </tr>
                                       @endforeach
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php $i++; ?>
               @endforeach
            @else
               <div class="text-center">
                  No Team Found <a href="{{ asset('/teams/list') }}">Click here to find from the team list.</a>
               </div>
            @endif

            @if (!empty($teams))
               <div class="text-align-center">
                  {{ Form::button('Apply', ['class' => 'btn btn-block-hope btn-orange', 'type' => 'submit']) }}
               </div>
            @endif
         {!! Form::close() !!}
      </div>
   </div>
@endsection;
@push('extra_scripts')
   <script src="{{ asset('public/js/Teams/index.js') }}"></script>
@endpush