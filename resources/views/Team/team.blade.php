@extends('layouts.user')

@section('title', 'Teams')

@section('content')
    <div class="container team-wraper">
        <div class="header-top clearfix">
            <div class="col-sm-4">
                <h1>Teams</h1>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="row">
                {!! Form::open(['url' => '/teams/list', 'method' => 'get', 'novalidate' => true]) !!}
                    <div class="col-sm-5">
                        {{ Form::label('division', 'Division') }}
                        <?php
                            echo Form::select('category_name', ['' => 'Select a category'] + $categories->toArray(), null, [
                                    'class' => 'form-control input-lg'
                                ]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?php
                            echo Form::button('Search', [
                                    'class' => 'btn btn-orange btn-lg',
                                    'type' => 'submit'
                                ]);
                        ?>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="250">@sortablelink('team_name', 'Team Name')</th>
                            <th>@sortablelink('category_name', 'Division')</th>
                            <th>@sortablelink('ppg', 'PPG')</th>
                            <th>@sortablelink('rpg', 'RPG')</th>
                            <th>@sortablelink('apg', 'APG')</th>
                            <th>@sortablelink('spg', 'SPG')</th>
                            <th>@sortablelink('bpg', 'BPG')</th>
                            <th>@sortablelink('fg', 'FG%')</th>
                            <th>@sortablelink('ft', 'FT%')</th>
                            <th>@sortablelink('3_pt_fg', '3-PT%')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($teams as $team)
                            <tr>
                                <td width="250">
                                    {{ $team->team_name }}
                                </td>
                                <td>
                                    {{ $team->category_name }}
                                </td>
                                <td>
                                    {{ $team->ppg }}
                                </td>
                                <td>
                                    {{ $team->rpg }}
                                </td>
                                <td>
                                    {{ $team->apg }}
                                </td>
                                <td>
                                    {{ $team->spg }}
                                </td>
                                <td>
                                    {{ $team->bpg }}
                                </td>
                                <td>
                                    {{ $team->fg }}
                                </td>
                                <td>
                                    {{ $team->ft }}
                                </td>
                                <td>
                                    {{ $team->three_pt_fg }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="11" class="text-center">
                                    No Record Found
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {!! $teams->appends($_GET)->render() !!}
            </div>
        </div>
    </div>
@endsection;