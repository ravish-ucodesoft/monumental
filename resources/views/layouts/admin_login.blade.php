<!DOCTYPE html>
<html>
    <head>
        <title>Monumental - @yield('title')</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="favicon.ico"/>
        @push('styles')
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/Administration/login.css') }}">
        @endpush
        @stack('styles')
    </head>
    <body>
        <div class="site-wrapper">
            <div class="site-wrapper-inner">
                <div class="container">
                    <div class="col-md-8 col-lg-offset-1 col-md-offset-1 login-inner-wraper padding-zero">
                        <div class="col-md-12 padding-zero">
                            @include('flash::message')
                        </div>
                        <div class="background-strip-top"></div>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        @push('scripts')
            <script src="{{ asset('public/js/jQuery-2.1.4.min.js') }}"></script>
            <script src="{{ asset('public/js/flash.js') }}"></script>
        @endpush
        @stack('scripts') 
    </body>
</html>