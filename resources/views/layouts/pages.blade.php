<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Monumental - @yield('title')</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="favicon.ico"/>
        @push('styles')
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/home.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/font-awesome.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/sticky-footer.css') }}">
            <link href="https://fonts.googleapis.com/css?family=Overpass:100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        @endpush
        @stack('styles')
        
    </head>
    <body class="">
        @section('header')
            @include('header_guest')
        @show

        {{-- <div class="container sign-in-section"> --}}
            @yield('content')
        {{-- </div> --}}

        @section('footer')
            @include('footer')
        @show
        
        @push('scripts')
            <script src="{{ asset('public/js/jQuery-2.1.4.min.js') }}"></script>
            <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        @endpush
        @stack('scripts')
        @stack('extra_scripts')
   </body>
</html>