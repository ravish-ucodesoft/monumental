<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Monumental - @yield('title')</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="favicon.ico"/>
        @push('styles')
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/vue-modal.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/Administration/style.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/Administration/AdminLTE.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/Administration/custom-switch.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery-ui.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/pnotify.custom.min.css') }}">
        @endpush
        @stack('styles')
        @stack('extrastyles')
        
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @section('header')
                @include('admin.header')
            @show
            @section('sidebar')
                @include('admin.sidebar')
            @show
            <div class="wrapper">
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper clearfix">
                    <div class="col-md-12 padding-zero">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>        
                                @endforeach
                            </div>
                        @endif
                        @include('flash::message')
                    </div>
                    @yield('content')
                </div>
            </div>
        </div>
        @include('admin.change_password')
        @include('admin.profile')
        @push('scripts')
            <script src="{{ asset('public/js/jQuery-2.1.4.min.js') }}"></script>
            <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('public/js/jquery.validate.js') }}"></script>
            <script src="{{ asset('public/js/additional-methods.js') }}"></script>
            <script src="{{ asset('public/js/admin-app.js') }}"></script>
            <script src="{{ asset('public/js/underscore-min.js') }}"></script>
            <script src="{{ asset('public/js/jquery-ui.js') }}"></script>
            <script src="{{ asset('public/js/common_validation_methods.js') }}"></script>
            <script src="{{ asset('public/js/pnotify.custom.min.js') }}"></script>
            <script src="{{ asset('public/js/vue.js') }}"></script>
            <script src="{{ asset('public/js/vue-resource.js') }}"></script>
            <script src="{{ asset('public/js/Administration/common.js') }}"></script>
            <script src="{{ asset('public/js/flash.js') }}"></script>
            <script src="{{ asset('public/js/Administration/navigation.js') }}"></script>
        @endpush
        @stack('scripts')
        @stack('extra_scripts')
   </body>
</html>